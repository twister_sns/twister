<?php

class Controller_Post extends Controller_Load{
	public function before(){
		parent::before();
		
		$this->template->content = View::forge('main/center/room/post');
		$this->template->left = View::forge('main/left/index');
		$this->template->right = false;
	}
	
	public function action_index($id = 1){
		$queryClass = new Class_Query();
		$queryClass->select = $id;
		$queryClass->control = 'single';
		$posts = $queryClass->queryOrder();
		$post = reset($posts['posts']);
		$postUser = [];
		$postUser['id'] = $post->user->id;
		$postUser['user_id'] = $post->user->user_id;
		$postUser['user_name'] = $post->user->user_name;
		
		$this->template->set_global('post',$post);
		$this->template->set_global('postUser',$postUser);
		$this->template->title = 'Twister';
		$this->template->navbar = View::forge('template_navbar');
	}
}