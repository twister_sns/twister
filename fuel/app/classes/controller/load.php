<?php

/*
 * 標準コントローラーを拡張します。APIを除く全てのコントローラーで使用されています。
 */

class Controller_Load extends Controller_Template{
	
	//テンプレート
	public $template = 'template_layout';
	
	public function before(){
		parent::before();
		
		$this->template->title = 'Twister';
		//$this->template->layout = View::forge('template_layout');
		$this->template->navbar = View::forge('template_navbar');
		$this->template->content = View::forge('main/center/room');
		$this->template->left = false;
		$this->template->right = false;
		
		$user = array();
		if(!Auth::check() AND Auth::check() == 0){
			//ゲスト情報を設定
			$user['id'] = 0;
			$user['user_id'] = 'kuzuho';
			$user['user_name'] = '';
			$this->template->set_global('navbar_mode','guest');
		}else{
			//登録情報を取得
			$user['id'] = Auth::get_uid();
			$user['user_id'] = Auth::get_user_id();
			$user['user_name'] = Auth::get_user_name();
			$user['user_desc'] = Auth::get_user_desc();
			//$user['user_desc'] = Auth::get('user_desc');
			$this->template->set_global('navbar_mode','user');
		}
		$this->template->set_global('user',$user);
		
		
		//ルームカテゴリを取得
		$room_categories = Model_Category::find('all', array(
			'where' => array(
				//array('id', '!=', 1),
			)
		));
		$this->template->set_global('room_categories', $room_categories);
		
		//ミュートユーザ
		$mute_users = Model_Mute::find('all', array(
			'where' => array(
				array('user_id', $user['id'])
			)
		));
		$this->template->set_global('mute_users', $mute_users);
	}
}
