<?php


class Controller_Top extends Controller_Load{
	
	public function before(){
		parent::before();
		$this->template->content = View::forge('top/index');
	}
	
	//public $template = 'template_';

	/* トップページ */
	public function action_index() {
		
		//ログイン済の場合メインへリダイレクト
		if(Auth::check()){
			Response::redirect('main');
		}
		
		$this->template->title = 'Twisterへようこそ！';
		$this->template->content = View::forge('top/index');
	}
	
	//ログイン・会員登録
	public function action_send() {
		$this->template->title = '認証';
		$this->template->content = View::forge('top/index');
		
		$auth = Auth::instance();
		$authMessage = '';
		
		//postされたデータを検証
		$validation = Validation::forge();
		if(Input::post('TCLType') == 'login'){
			$validation->add_field('user_id', 'ユーザID', 'required|min_length[2]|max_length[32]');
			$validation->add_field('user_pass', 'パスワード', 'required|min_length[4]|max_length[128]');
		}elseif(Input::post('TCLType') == 'regist'){
			$validation->add_field('user_name', 'ユーザ名', 'required|min_length[1]|max_length[64]');
			$validation->add_field('user_id', 'ユーザID', 'required|min_length[2]|max_length[32]|match_pattern[/^[a-zA-Z0-9-_]*$/]');
			$validation->add_field('user_pass', 'パスワード', 'required|min_length[4]|max_length[128]|match_pattern[/^[a-zA-Z0-9]*$/]');
			$validation->add_field('user_pass_retype', 'パスワード', 'required|match_field[user_pass]|match_pattern[/^[a-zA-Z0-9]*$/]');
		}
		
		//検証に成功
		if($validation->run()){
			//会員登録・ログイン。成功したらメインへリダイレクト
				if(Input::post('TCLType') == 'regist'){
					$authMessage = '管理者にお問合せ下さい。';
						$auth->create_user(Input::post('user_id'), Input::post('user_pass') ,Input::post('user_name'));
						if(Auth::login(Input::post('user_id'), Input::post('user_pass'))){
							Response::redirect('main');
						}
				}elseif(Input::post('TCLType') == 'login'){
					if(Auth::login(Input::post('user_id'), Input::post('user_pass'))){
						Response::redirect('main');
					}
				}
		}else{
			foreach($validation->error() as $error){
				$authMessage .= $error.'<br>';
			}
		}
		if(!isset($authMessage)){$authMessage = '管理者にお問合せ下さい。';}
		$this->template->set_global('authMessage',$authMessage,false);
	}
	
	/* ログアウト */
	public function action_logout(){
		Auth::logout();
		Response::redirect('/');
	}
}
