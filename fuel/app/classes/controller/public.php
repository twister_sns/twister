<?php

/*
 * パブリック
 */

class Controller_Public extends Controller_Load{
	
	public function before(){
		parent::before();
		$this->template->content = View::forge('main/center/room');
		$this->template->left = View::forge('main/left/index');
	}
	
	//トップページ
	public function action_index() {
		$this->template->title = 'パブリック';
		$room = Model_Room::find(1);
		
		//投稿
		$posts = Model_Post::find('all', array(
			'where' => array(
				array('del', 1),
			),
			'order_by' => array('id' => 'desc'),
			'limit' => 80
		));
		
		$this->template->set_global('posts', $posts);
		$this->template->set_global('room', $room);
	}
}
