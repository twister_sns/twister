<?php

/*
 * ルーム
 */

class Controller_Room extends Controller_Load{
	
	public function before(){
		parent::before();
		$this->template->content = View::forge('main/center/room');
		$this->template->left = View::forge('main/left/room/index');
	}

	//トップページ
	public function action_index($param_room_name = null) {
		
		$roomClass = new Class_Room();
		$postClass = new Class_Post();
		$userClass = new Class_User();
		
		if($param_room_name != null){
			$roomClass->room_name = $param_room_name;
			$room_id = $roomClass->find_room_id();
			$room = Model_Room::find($room_id);
			if(!isset($room)){
				Response::redirect('main');
			}

			$posts = Model_Post::find('all', array(
				'where' => array(
					array('id', $room_id),
					array('del', 0),
				),
				'order_by' => array('id' => 'desc'),
				'limit' => 80
			));
			$this->template->set_global('posts', $posts);
			$this->template->title = $room->room_name;
		}else{
			//$room = Model_Room::find(1);
			$room = Model_Room::find($param_room_name);
			$rooms = Model_Room::find('all', array(
				'related' => array(
					'post' => array(
						'where' => array(
							array('del' => 0),
						),
						'order_by' => array(
							'created_at' => 'desc'
						),
					),
				),
				'where' => array(
					array('id', '!=', 1),
					array('room_hidden', 0),
					array('room_del', 0),
				),
				'rows_limit' => 5,
			));
			$this->template->content = View::forge('main/center/room/index');
			$this->template->title = 'ルーム';
		}
		$this->template->set_global('room', $room);
	}
	
	//カテゴリ検索
	public function action_category($param_room_id = 1) {
			$room_category = Model_Category::find($param_room_id);
			$rooms = Model_Room::find('all', array(
				'where' => array(
					array('id', '!=', 1),
					array('room_category', $param_room_id),
					array('room_hidden', 0),
					array('room_del', 0),
					//array('room_id', $param_room_id),
					//array('del', 0),
				),
				'order_by' => array('id' => 'desc'),
				'limit' => 80
			));
			$this->template->content = View::forge('main/center/room/index');
			$this->template->set_global('rooms', $rooms);
			$this->template->set_global('room_category', $room_category);
			$this->template->title = 'ルーム検索';
	}
}