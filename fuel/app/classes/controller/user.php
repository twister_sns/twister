<?php

/*
 * ユーザーページ
 */

class Controller_User extends Controller_Load{
	function before(){
		parent::before();
		
		$this->template->top = View::forge('main/top/user/index');
		$this->template->content = View::forge('main/center/user/index');
		$this->template->left = View::forge('main/left/user/index');
	}
	
	function action_index($id = 'kuzuho', $mode = 'post'){
		$user_info = Model_User::find('all',array(
			'where' => array(
				array('user_id',$id)
			)
		));
		$user_info = reset($user_info);
		
		$mute = Model_Mute::find('all',array(
			'where' => array(
				array('user_id', Auth::get_uid()),
				array('block_id', $user_info->id)
			)
		));
		$mute = reset($mute);
		
		if(is_object($mute)){
			$user_block = true;
		}else{
			$user_block = false;	
		}
		
		$this->template->set_global('user', $user_info);
		$this->template->title = $user_info->user_name;
		$this->template->set_global('user_block', $user_block);
	}
}