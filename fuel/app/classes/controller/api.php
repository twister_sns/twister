<?php

/**
 * API
 *  Ajax経由で送信されたリクエストを処理します。
 */

class Controller_Api extends Controller_Rest{
  private $query;
  private $post;
  private $user;
  private $action;

  public function before(){
		parent::before();

    //クラス読込
		$this->post = new Class_Post();
		$this->query = new Class_Query();
		$this->user = new Class_User();
		$this->action = new Class_Action();
    $this->file = new Class_File();
	}

	/*
	 * 投稿内容をDBへ保存
	 *  投稿された内容をデータベースに保存します。
	 * @method POST
	 * @param room_id
	 * @param message
	 * @return result (bool)
	 * @return message (string)
	 */
	public function post_Room_Post(){
		if(!Auth::check() OR Auth::get_uid() == 0){
			throw new HttpNotFoundException;
		}

		$room_id = (Input::get('room_id') == 'all') ? 1 :Input::post('room_id');
		$message = htmlspecialchars(Input::post('message'), ENT_QUOTES).htmlspecialchars(Input::post('add_files'), ENT_QUOTES);

		$user = Model_User::find(Auth::get_uid());
		$user->user_exp = $user->user_exp + $this->post->calcExp(Input::post('message'));
		$user->save();

    $post = Model_Post::forge();
		$post->user_id = Auth::get_uid();
		$post->room_id = $room_id;
		$post->message = $message;
		$post->ip = $_SERVER['REMOTE_ADDR'];
		$post->host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$post->del = 0;
		if($post->save()){
			return array('result' => true, 'message' => '投稿が完了しました！');
		}else{
			return array('result' => false, 'message' => '投稿情報が読み取れません');
		}
	}

	/*
   * [要修正]
	 * ルームコントロール
	 *  ルームを作成、編集します。
	 * @method POST
	 * @param room_id
	 * @param room_name
	 * @param room_desc
	 * @param room_hidden
	 * @param form_room_category
	 * @return result (bool)
	 * @return message (string)
	 */
	public function post_Room_Control(){
		if(!Auth::check() OR Auth::get_uid() == 0){
			throw new HttpNotFoundException;
		}
		if(Input::post('mode') == 'create'){
			// ルーム作成
			$hidden = (Input::post('room_hidden') == 'hidden') ? 1 : 0;
			if(Input::post('form_room_category') == '1'){$category = 2;}else{$category = Input::post('form_room_category');}
			$room = Model_Room::forge();
			$room->user_id = Auth::get_uid();
			$room->room_name = htmlspecialchars(Input::post('room_name'), ENT_QUOTES);
			$room->room_desc = htmlspecialchars(Input::post('room_desc'), ENT_QUOTES);
			$room->room_hidden = $hidden;
			$room->room_category = $category;
			$room->room_del = 0;
			if($room->save()){
				return array('result' => true, 'message' => '作成完了!', 'return' => htmlspecialchars(Input::post('room_name'), ENT_QUOTES));
			}else{
				return array('result' => false, 'message' => '投稿情報が読み取れません');
			}
		}elseif(Input::post('mode') == 'edit'){
			// ルーム編集
			$hidden = (Input::post('room_hidden') == 'hidden') ? 1 : 0;
			if(Input::post('form_room_category') == '1'){$category = 2;}else{$category = Input::post('form_room_category');}
			$room = Model_Room::find(Input::post('room_id'));
			if($room->user_id != Auth::get_uid()){
				return array('result' => false, 'message' => '認証ができません');
			}
			$room->room_name = htmlspecialchars(Input::post('room_name'), ENT_QUOTES);
			$room->room_desc = htmlspecialchars(Input::post('room_desc'), ENT_QUOTES);
			$room->room_category = $category;
			$room->room_hidden = $hidden;
			if($room->save()){
				//$rooms = Model_Room::find(Input::post('room_id'));
				return array('result' => true, 'message' => '編集が完了しました');
			}else{
				return array('result' => false, 'message' => '編集が完了できませんでした。認証状態が解除されていませんか？');
			}
		}
	}

	/*
	 * ★使用されていません
	 *  ミュートユーザーの取得
	 *  ミュートユーザーを配列で取得します。
	 *  取得した情報はミュート機能で利用されます。
	 * @method GET
	 */
	public function get_Mute_User(){
		$mutes = Model_Mute::find('all', array(
			'where' => array(
				array('user_id', 1),
			),
			//'order_by' => array('id' => 'desc'),
			//'limit' => $this->limit
		));
		echo DB::last_query();
		return $mutes;
	}

	/*
	 * ルーム投稿読込
	 *  ルームの投稿を出力します。
	 *  Queryクラス で投稿取得した後、出力用に整形します。
	 * @method GET
	 * @param select
	 * @param control
	 * @param mode
	 * @param message
	 * @param page
	 * @return result (bool)
	 * @return return (array)
	 * @return rows (int)
	 */
	public function get_Read_Post(){
    // パラメーターが空文字の場合
    $control = (empty(Input::get('control'))) ? 'main' : Input::get('control', 'main');
    $select = (empty(Input::get('select'))) ? 1 : Input::get('select', 1);
		$mode = (empty(Input::get('mode'))) ? 'post' : Input::get('mode', 'post');
		$page = (empty(Input::get('page'))) ? 1 : Input::get('page', 1);

		//投稿取得
		$this->query->control = $control;
		$this->query->select = $select;
		$this->query->mode = $mode;
		$this->query->page = $page;
		$posts = $this->query->queryOrder();

		//投稿を配列へ整形
		if(isset($posts) && array_key_exists('posts', $posts) && is_array($posts['posts'])){
			foreach($posts['posts'] as $key=>$post){
				$return[$key]['id']		= $post->id;
				$return[$key]['date']	= $post->created_at;

				//post
				$return[$key]['post']['system']	= $post->post_system;
				$return[$key]['post']['message'] = nl2br($this->post->postMake($post));
				$return[$key]['post']['good']	= $post->good;
				$return[$key]['post']['favorite']	= $post->favorite;

				//user
				$return[$key]['user']['uid']		= $post->user_id;
				$return[$key]['user']['id']		= $post->user->user_id;
				$return[$key]['user']['name']	= $post->user->user_name;
				$return[$key]['user']['level']	= $this->user->toLevel($post->user->user_exp);
				$return[$key]['user']['exp']	= $post->user->user_exp;
				$return[$key]['user']['my'] = ($post->user->user_id == Auth::get_user_id()) ? 1:0;

				//room
				$return[$key]['room']['id']		= $post->room_id;
				$return[$key]['room']['name']	= $post->room->room_name;
			}
			rsort($return);
		}else{
			$return = false;
			$posts['rows'] = 0;
		}
		return array('result'=>true, 'return'=>$return, 'rows'=>$posts['rows']);
	}

	/*
	 * ルーム情報読込
	 *  ルームの情報を出力します。
	 *  Queryクラス でルーム情報を取得した後、出力用に整形します。
	 * @method GET
	 * @param select
	 * @param mode
	 * @param page
	 * @return result (bool)
	 * @return return (array)
	 * @return rows (int)
	 */
	public function get_Read_Room(){
		$select = (Input::get('select') == '') ? 1 : Input::get('select',3);
		$mode = (Input::get('mode') == '') ? 'post' : Input::get('mode','post');

		//投稿取得
		$this->query->control = 'roomList';
		$this->query->select = $select;
		$this->query->mode = $mode;
		$this->query->page = Input::get('page', 1);
		$rooms = $this->query->queryOrder();

		//投稿を配列へ整形
		$return = [];
		if(isset($rooms) && array_key_exists('return', $rooms) && is_array($rooms['return'])){
			foreach($rooms['return'] as $key=>$room){

				//rooms テーブル
				$return[$key]['room']['id']		= $room->id;
				$return[$key]['room']['name']	= $room->room_name;
				$return[$key]['room']['desc']	= $room->room_desc;
				$return[$key]['room']['date']	= $room->created_at;
				$return[$key]['room']['category']	= $room->category->category;

				//users リレーションテーブル
				$return[$key]['user']['uid']		= $room->user_id;
				$return[$key]['user']['id']		= $this->user->get_user_id($room->user_id);
				$return[$key]['user']['name']	= $this->user->get_user_name($room->user_id);

				//posts リレーションテーブル
				$return[$key]['post']['id']		= $room->post->id;
				$return[$key]['post']['created_at']		= $room->post->created_at;

				//goods リレーションテーブル
				//$return[$fcount]['good']	= $room->good;
			}
			rsort($return);
		}else{
			$return = false;
			$rooms['rows'] = 0;
		}
		return array('result'=>true, 'return'=>$return, 'rows'=>$rooms['rows']);
	}

	/*
	 * ファイルアップロード
	 *  投稿フォームにてAJAXで送信されたファイルを処理します。
	 *  ユーザー設定のサムネイルや背景画像は[class/file.php]で処理します。
	 * @method POST
	 * @return result (bool)
	 * @return upload_id (int)
	 */
	public function post_file_upload(){

		Upload::process();
		if (Upload::is_valid()){
			Upload::save();

			foreach (Upload::get_files() as $key => $file){

				//ファイル名単体
				$file_id = explode('.', $file['saved_as']);
				$file_id = $file_id[0];

				$file['extension'] = strtolower($file['extension']);
				$file['extension'] = ($file['extension'] == 'jpeg') ? 'jpg' : $file['extension'];

				//経験値の追加
				$user = Model_User::find(Auth::get_uid());
				$user->user_exp = $user->user_exp + 20 + floor($file['size'] / 10000);
				$user->save();

				//保存
				$upload = Model_Upload::forge();
				$upload->upload_id = $file_id;
				$upload->user_id = Auth::get_uid();
				$upload->random_id = '123456789012';
				$upload->upload_name = $file['name'];
				$upload->upload_ext = $file['extension'];
				$upload->upload_size = $file['size'];
				$upload->upload_count = 0;
				$upload->upload_ip = $_SERVER['REMOTE_ADDR'];
				$upload->upload_host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$upload->save();

        $this->file->file['id'] = $file_id;
				$this->file->file['extension'] = $file['extension'];
        if($file['extension'] == 'jpg' or $file['extension'] == 'jpeg' or $file['extension'] == 'png' or $file['extension'] == 'gif' or $file['extension'] == 'bmp'){
					$this->file->uploadImageThumbnail();
				}
				if($file['extension'] == 'mp4' or $file['extension'] == 'avi' or $file['extension'] == 'flv' or $file['extension'] == 'wmv' or $file['extension'] == 'mkv' or $file['extension'] == 'mov' or $file['extension'] == 'webm' or $file['extension'] == 'mpg' or $file['extension'] == 'mpeg'){
					$this->file->uploadVideoThumbnail();
				}

				return array('result'=>true, 'file_name'=>$file['saved_as'], 'upload_id'=>$file_id);
			}
		}
	}

	/*
	 * ユーザーステータス
	 *  @param mode によって取得する情報を切り替えます。
	 * @method get
	 * @return result (bool)
	 * @return return (array)
	 */
	public function get_user_status(){
		$this->user->id = Input::get('id', 1);

		switch (Input::get('mode')){
			case 'profile_status':
				$return = $this->user->preProfileStatus();
				break;
			case 'user_status':
				$return = $this->user->preUserStatus();
				break;
		}

		return array('result'=>true, 'return'=>$return);
	}

	/*
	 * ユーザーアクション
	 *  ルーム投稿のGood, Favoriteなどを処理します。
	 *  ユーザーのミュート処理もここで行います。
	 * @method post
	 * @param action
	 * @param value
	 * @return return (bool)
	 * @return result (array)
	 */
	public function get_user_action(){
		$action = Input::get('action');
		$value = Input::get('value','');
		$this->action->id = Auth::get_uid();
		$this->action->value = $value;

		switch ($action){
			case 'block':
				$result = $this->action->userBlock();
				break;
			case 'good':
				$result = $this->action->userGood();
				break;
			case 'favorite':
				$result = $this->action->userFavorite();
				break;
			case 'delete':
				$result = $this->action->userDelete();
				break;
			default:
				exit();
		}
		return array('return'=>true, 'result'=>$result);
	}
}