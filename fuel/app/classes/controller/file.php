<?php

/*
 * アップロードされたファイルを読むこむ際に呼び出されます。
 */

class Controller_File extends Controller{
	public function before(){
		parent::before();
	}
	
	public function action_index($upload_id = false){
		
		//パラメータの処理
		$upload_id = explode('.', $upload_id);
		$upload_id = $upload_id[0];
		
		//ファイル検索
		$upload_file = Model_Upload::find('all', array(
			'where' => array(
				array('upload_id', $upload_id)
			)
		));
		if($upload_file == NULL){
			throw new HttpNotFoundException;
		}
		
		//キー・要素を取得
		$upload_key = key($upload_file);
		$upload_file = reset($upload_file);
		
		//ファイルカウント
		if(Auth::get_uid() != $upload_file['user_id']){
			$upload_count = Model_Upload::find($upload_key);
			$upload_count->upload_count = $upload_count->upload_count+1;
			$upload_count->save();
		}
		
		//閲覧者情報
		$download = Model_Download::forge();
		$download->download_id = $upload_id;
		$download->download_ua = $_SERVER['HTTP_USER_AGENT'];
		$download->download_ip = $_SERVER['REMOTE_ADDR'];
		$download->download_host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$download->save();
		
		Response::redirect('http://data.kzho.net/up/sorce/'.$upload_file['upload_id'].'.'.$upload_file['upload_ext']);
	}
}