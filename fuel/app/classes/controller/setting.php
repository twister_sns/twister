<?php

/*
 * 設定
 */

class Controller_Setting extends Controller_Load{
	public function before(){
		parent::before();
		
		if(!Auth::check()){
			Response::redirect('top');
		}
		
		$this->template->left = View::forge('main/left/setting/index');
		//$this->template->left = false;
		//$this->template->content = View::forge('main/center/setting/index');
	}
	
	public function action_index(){
		Response::redirect('setting/profile');
	}
	
	public function action_profile(){
		//$setting = Model_setting::forge();
		
		if( Input::method() == 'POST' ) {
			$setting = array();
			$setting['user_name'] = htmlspecialchars(Input::post('user_name'), ENT_QUOTES);
			$setting['user_desc'] = htmlspecialchars(Input::post('user_desc'), ENT_QUOTES);
			$setting['user_icon'] = Input::file('user_icon',false);
			$setting['user_background'] = Input::file('user_background',false);
			
			$model = Model_User::find(Auth::get_uid());
			$model->user_name = $setting['user_name'];
			$model->user_desc = $setting['user_desc'];
			if($model->save()){
				$outMessage = '設定を適用しました';
			}else{
				$outMessage = '設定が適用できませんでした';
			}

			if(!$setting['user_icon']['error']){
				$convert = new Class_File();
				$convert->user_id = Auth::get_uid();
				$convert->temp = $setting['user_icon']['tmp_name'];
				$convert->userIcon();
				$convert = null;
			}
			
			if(!$setting['user_background']['error']){
				$convert = new Class_File();
				$convert->user_id = Auth::get_uid();
				$convert->temp = $setting['user_background']['tmp_name'];
				$convert->mode = 'user_background';
				$convert->userBackground();
				$convert = null;
			}
			$this->template->set_global('outMessage', $outMessage);
			echo '<META HTTP-EQUIV="REFRESH" CONTENT="0;URL='.Uri::current().'">';
		}
		$this->template->title = 'プロフィールの編集';
		$this->template->content = View::forge('main/center/setting/profile');
	}

	public function action_password(){
		$this->template->title = 'パスワードの変更';
		$this->template->content = View::forge('main/center/setting/password');
		$outMessage = '';
		
		if( Input::method() == 'POST' ) {
			if(Input::post('user_new_pass') == Input::post('user_new_pass_re')){
				if(Auth::change_password(Input::post('user_old_pass'), Input::post('user_new_pass'))){
					$outMessage = '変更が完了しました。';
				}else{
					$outMessage = '変更できませんでした。';
				}
			}else{
				$outMessage = '新しいパスワード、再入力パスワードが一致しません。';
			}
			
			$this->template->set_global('outMessage', $outMessage);
			//echo '<META HTTP-EQUIV="REFRESH" CONTENT="0;URL='.Uri::current().'">';
		}
	}

}