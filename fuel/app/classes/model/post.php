<?php

class Model_Post extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'user_id',
		'room_id',
		'post_system',
		'message',
		'del',
		'ip',
		'host',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'posts';

	//投稿関連情報
	protected static $_belongs_to = array(
		'user' => array(
			'key_from' => 'user_id',
			'model_to' => 'Model_User',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false
		),
		'room' => array(
			'key_from' => 'room_id',
			'model_to' => 'Model_Room',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false
		),
	);
	
	protected static $_has_many = array(
		'good' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Good',
			'key_to' => 'post_id',
			'cascade_save' => false,
			'cascade_delete' => true,
		),
		'favorite' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Favorite',
			'key_to' => 'post_id',
			'cascade_save' => false,
			'cascade_delete' => true,
		)
	);
	
	
	//出力時に除外するカラム
	protected static $_to_array_exclude = array(
		'ip', 'host', 'del'
	);
}
