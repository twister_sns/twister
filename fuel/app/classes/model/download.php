<?php

class Model_Download extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'download_id',
		'user_id',
		'random_id',
		'download_ua',
		'download_ip',
		'download_host',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'downloads';

}
