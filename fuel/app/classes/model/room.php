<?php

class Model_Room extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'room_name',
		'room_desc',
		'user_id',
		'room_category',
		'room_hidden',
		'room_del',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'rooms';

	//関連情報
	protected static $_belongs_to = array(
		
		'user' => array(
			'key_from' => 'user_id',
			'model_to' => 'Model_User',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false
		),
		
		'category' => array(
			'key_from' => 'room_category',
			'model_to' => 'Model_Category',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false
		)
		
	);
	
	//投稿
	protected static $_has_one = array(
		
		'post' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Post',
			'key_to' => 'room_id',
			'cascade_save' => true,
			'cascade_delete' => false,
		),
		
	);
	

}
