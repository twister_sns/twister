<?php

class Model_User extends \Orm\Model{
	protected static $_properties = array(
		'id',
		'user_id',
		'user_name',
		'user_desc',
		'user_exp',
		'user_pass',
		'group',
		'last_login',
		'login_hash',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'users';
	
	/*
	protected static $_has_many = array(
		//ミュート テーブル
		
		'mute' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Mute',
			'key_to' => 'user_id',
			'cascade_save' => false,
			'cascade_delete' => false,
		)
	);
		 * 
		 */

}
