<?php

/*
 * ユーザーアクション
 *  各投稿の削除・ファボ、ユーザーのミュート処理を行います。
 */

class Class_Action{
	public $id;
	public $action;
	public $value;

	/*
	 * ユーザーブロック
	 *  Mute テーブルでミュートの有無を判断し処理を分岐します。
	 * @return return (bool)
	 */
	public function userBlock(){
		$userClass = new Class_User();
		if($this->value == $this->id){
			return false;
		}
		if($this->id == 0){
			return false;
		}

		//ユーザーIDを取得
		$user_id = $userClass->get_user_id($this->value);
		//重複確認
		$dup = Model_Mute::find('all', array(
			'where' => array(
				array('user_id', $this->id),
				array('block_id', $this->value)
			)
		));
		$dup = reset($dup);

		//偽→追加、真→削除
		if(!is_object($dup)){
			$mute = Model_Mute::forge();
			$mute->user_id = $this->id;
			$mute->block_id = $this->value;
			if($mute->save()){
				//ブロックした旨を投稿
				$post = Model_Post::forge();
				$post->user_id = $this->id;
				$post->message = '@'.$user_id.' [b]をブロックしました。[/b]';
				$post->room_id = 1;
				$post->post_system = 1;
				$post->del = 0;
				$post->ip = $_SERVER['REMOTE_ADDR'];
				$post->host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$post->save();
				return true;
			}else{
				return false;
			}
		}else{
			if($dup->delete()){
				//ブロックした旨を投稿
				$post = Model_Post::forge();
				$post->user_id = $this->id;
				$post->message = '@'.$user_id.' [b]のブロックを解除しました。[/b]';
				$post->room_id = 1;
				$post->post_system = 1;
				$post->del = 0;
				$post->ip = $_SERVER['REMOTE_ADDR'];
				$post->host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$post->save();
				return true;
			}else{
				return false;
			}
		}
	}

	/*
	 * Good
	 * @return return (int||bool)
	 */
	public function userGood(){
		$userClass = new Class_User();
		$good_id = $userClass->get_post_user_id($this->value);

		//重複確認
		$dup = Model_Good::find('all', array(
			'where' => array(
				array('user_id', $this->id),
				array('post_id', $this->value),
			)
		));
		$dup = reset($dup);

		//偽→追加、真→削除
		if(!is_object($dup)){
			$mute = Model_Good::forge();
			$mute->user_id = $this->id;
			$mute->post_id = $this->value;
			$mute->good_id = $good_id;
			if($mute->save()){
				return 1;
			}else{
				return false;
			}
		}else{
			if($dup->delete()){
				return 2;
			}else{
				return false;
			}
		}
	}

	/*
	 * ふぁぼ
	 * @return return (int||bool)
	 */
	public function userFavorite(){
		$userClass = new Class_User();
		$fav_id = $userClass->get_post_user_id($this->value);

		//重複確認
		$dup = Model_Favorite::find('all', array(
			'where' => array(
				array('user_id', $this->id),
				array('post_id', $this->value),
			)
		));
		$dup = reset($dup);

		//追加、削除
		if(!is_object($dup)){
			$mute = Model_Favorite::forge();
			$mute->user_id = $this->id;
			$mute->post_id = $this->value;
			$mute->fav_id = $fav_id;
			if($mute->save()){
				return 1;
			}else{
				return false;
			}
		}else{
			if($dup->delete()){
				return 2;
			}else{
				return false;
			}
		}
	}

	/*
	 * 削除
	 * @return return (bool)
	 */
	public function userDelete(){
		//本人確認
		$dup = Model_Post::find('all', array(
			'where' => array(
				array('user_id', $this->id),
				array('id', $this->value),
			)
		));
		$dup = reset($dup);

		if(is_object($dup)){
			$post = Model_Post::find($this->value);
			//print_r($post);
			$post->del = 1;
			if($post->save()){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}