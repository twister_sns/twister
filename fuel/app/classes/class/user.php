<?php

/*
 * ユーザー関連
 *  ユーザー検索、情報取得に関する処理
 */

class Class_User{

  public $id;

	/*
	 * プロフィール用ステータス取得
	 * @return (array)
	 */
	public function preProfileStatus(){
		$return = [];
		$return['post'] = $this->getPost();
		$return['exp'] = $this->getExp();
		$return['level'] = $this->getLevel($return['exp']);
		return $return;
	}

	/*
	 * ★未定
	 *  $this->preProfileStatus() と同じ動作
	 * @return (array)
	 */
	public function preUserStatus(){
		$return = [];
		$return['post'] = $this->getPost();
		$return['exp'] = $this->getExp();
		$return['level'] = $this->getLevel($return['exp']);
		return $return;
	}

	/*
	 * 投稿数
	 * @return (int)
	 */
	public function getPost(){
		$query = Model_Post::query()->where('user_id', $this->id)->where('del', 0);
		return $query->count();
	}

	/*
	 * 経験値
	 * @return (int)
	 */
	public function getExp(){
		$query = Model_User::query()->select('user_exp')->where('id', $this->id)->get();
		$query = reset($query);
		return is_object($query) ? $query->user_exp : 0;
	}

	/*
	 * レベル
	 * @return (int||string)
	 */
	public function getLevel($exp){
    $return = $this->toLevel($exp);
		return $return;
	}

	/*
	 * Good
	 * @return (int)
	 */
	public function getWasGood(){
		$query = Model_Good::query()->where('user_id', $this->id);
		return $query->count();
	}

	/*
	 * 被Good
	 * @return (int)
	 */
	public function getHasGood(){
		$query = Model_Good::query()->where('good_id', $this->id);
		return $query->count();
	}

	/*
	 * ふぁぼ
	 * @return (int)
	 */
	public function getWasFav(){
		$query = Model_Favorite::query()->where('user_id', $this->id);
		return $query->count();
	}

	/*
	 * 被ふぁぼ
	 * @return (int)
	 */
	public function getHasFav(){
		$query = Model_Favorite::query()->where('fav_id', $this->id);
		return $query->count();
	}



  /*
   * STATIC FUNCTION
   */

	/*
	 * ユーザーNUM取得
	 * @param (string)
	 * @return (int||bool)
	 */
	public static function get_id($user_id){
		$user = Model_User::find('all',array(
			'where' => array(
				array('user_id', $user_id)
			)
		));
		return ($user->id) ? $user->id : false;
	}

	/*
	 * ユーザーID取得
	 * @param (int)
	 * @return (int||bool)
	 */
	public static function get_user_id($id){
		$user = Model_User::find('all',array(
			'where' => array(
				array('id', $id)
			)
		));
		$user = reset($user);
		return ($user->user_id) ? $user->user_id : false;
	}

	/*
	 * ユーザー名取得
	 * @param (int)
	 * @return (string||bool)
	 */
	public static function get_user_name($id){
		$user = Model_User::find('all', array(
			'where' => array(
				array('id', $id)
			)
		));
		$user = reset($user);
		return ($user->user_name) ? $user->user_name : false;
	}

	/*
	 * ユーザー情報取得
	 * @param (int)
	 * @return (array)
	 */
	public static function get_user_array($id){
		$user = Model_User::find('all', array(
			'where' => array(
				array('id', $id)
			)
		));
		$user = reset($user);
		return $user;
	}

	/*
	 * 投稿ユーザーID取得
	 * @param (int)
	 * @return (string)
	 */
	public static function get_post_user_id($post_id){
		$user = Model_Post::find('all', array(
			'where' => array(
				array('id', $post_id)
			)
		));
		$user = reset($user);
		return $user->user_id;
	}

	/*
	 * レベル計算
	 * @param (int)
	 * @return (int||string)
	 */
	public static function toLevel($exp){
		if($exp <= 99){
			return 0;
		}elseif($exp <= 200){
			return 1;
		}elseif($exp <= 400){
			return 2;
		}elseif($exp <= 800){
			return 3;
		}elseif($exp <= 1100){
			return 4;
		}elseif($exp <= 1400){
			return 5;
		}elseif($exp <= 1700){
			return 6;
		}elseif($exp <= 2000){
			return 7;
		}elseif($exp <= 2500){
			return 8;
		}elseif($exp <= 3000){
			return 9;
		}elseif($exp <= 3500){
			return 10;
		}elseif($exp <= 5000){
			return 11;
		}elseif($exp <= 7000){
			return 12;
		}elseif($exp <= 9000){
			return 13;
		}elseif($exp <= 11000){
			return 14;
		}elseif($exp <= 14000){
			return 15;
		}elseif($exp <= 17000){
			return 16;
		}elseif($exp <= 20000){
			return 17;
		}elseif($exp <= 24000){
			return 18;
		}elseif($exp <= 28000){
			return 19;
		}elseif($exp <= 32000){
			return 20;
		}elseif($exp <= 37000){
			return 21;
		}elseif($exp <= 42000){
			return 22;
		}elseif($exp <= 47000){
			return 23;
		}elseif($exp <= 53000){
			return 24;
		}elseif($exp <= 59000){
			return 25;
		}elseif($exp <= 65000){
			return 26;
		}elseif($exp <= 71000){
			return 27;
		}elseif($exp <= 78000){
			return 28;
		}elseif($exp <= 85000){
			return 29;
		}elseif($exp <= 92000){
			return 30;
		}elseif($exp <= 100000){
			return 31;
		}elseif($exp <= 110000){
			return 32;
		}elseif($exp <= 120000){
			return 33;
		}elseif($exp <= 130000){
			return 34;
		}elseif($exp <= 140000){
			return 35;
		}elseif($exp <= 150000){
			return 36;
		}elseif($exp <= 170000){
			return 37;
		}elseif($exp <= 190000){
			return 38;
		}elseif($exp <= 210000){
			return 39;
		}elseif($exp <= 240000){
			return 40;
		}elseif($exp <= 270000){
			return 41;
		}elseif($exp <= 300000){
			return 42;
		}elseif($exp <= 340000){
			return 43;
		}elseif($exp <= 380000){
			return 44;
		}elseif($exp <= 420000){
			return 45;
		}elseif($exp <= 470000){
			return 46;
		}elseif($exp <= 520000){
			return 47;
		}elseif($exp <= 570000){
			return 48;
		}elseif($exp <= 630000){
			return 49;
		}elseif($exp <= 690000){
			return 50;
		}elseif($exp <= 750000){
			return 51;
		}elseif($exp <= 820000){
			return 52;
		}elseif($exp <= 890000){
			return 53;
		}elseif($exp <= 960000){
			return 54;
		}elseif($exp <= 1040000){
			return 55;
		}elseif($exp <= 1120000){
			return 56;
		}elseif($exp <= 1200000){
			return 57;
		}elseif($exp <= 1290000){
			return 58;
		}elseif($exp <= 1380000){
			return 59;
		}elseif($exp <= 1470000){
			return 60;
		}elseif($exp <= 1600000){
			return 61;
		}elseif($exp <= 1800000){
			return 62;
		}elseif($exp <= 2000000){
			return 63;
		}elseif($exp <= 2500000){
			return 64;
		}elseif($exp <= 3000000){
			return 65;
		}elseif($exp <= 3500000){
			return 66;
		}elseif($exp <= 4000000){
			return 67;
		}elseif($exp <= 4500000){
			return 68;
		}elseif($exp <= 5000000){
			return 69;
		}elseif($exp <= 5500000){
			return 70;
		}elseif($exp <= 6000000){
			return 71;
		}elseif($exp <= 6500000){
			return 72;
		}elseif($exp <= 7000000){
			return 73;
		}elseif($exp <= 7500000){
			return 74;
		}elseif($exp <= 8000000){
			return 75;
		}elseif($exp <= 8500000){
			return 76;
		}elseif($exp <= 9000000){
			return 77;
		}elseif($exp <= 9500000){
			return 78;
		}elseif($exp <= 10000000){
			return 79;
		}elseif($exp <= 15000000){
			return 80;
		}elseif($exp <= 20000000){
			return 81;
		}elseif($exp <= 25000000){
			return 82;
		}elseif($exp <= 30000000){
			return 83;
		}elseif($exp <= 35000000){
			return 84;
		}elseif($exp <= 40000000){
			return 85;
		}elseif($exp <= 45000000){
			return 86;
		}elseif($exp <= 50000000){
			return 87;
		}elseif($exp <= 55000000){
			return 88;
		}elseif($exp <= 60000000){
			return 89;
		}elseif($exp <= 65000000){
			return 90;
		}elseif($exp <= 70000000){
			return 91;
		}elseif($exp <= 75000000){
			return 92;
		}elseif($exp <= 80000000){
			return 93;
		}elseif($exp <= 85000000){
			return 94;
		}elseif($exp <= 90000000){
			return 95;
		}elseif($exp <= 95000000){
			return 96;
		}elseif($exp <= 96000000){
			return 97;
		}elseif($exp <= 97000000){
			return 98;
		}elseif($exp <= 98000000){
			return 99;
		}elseif($exp <= 99000000){
			return 100;
		}elseif($exp <= 99900000){
			return 101;
		}elseif($exp <= 99990000){
			return 102;
		}elseif($exp <= 99999000){
			return 103;
		}elseif($exp <= 99999900){
			return 104;
		}elseif($exp <= 99999990){
			return 105;
		}elseif($exp <= 100000000){
			return 106;
		}else{
			return 'MAX';
		}
	}
}