<?php

/*
 * 投稿関連クラス
 *  投稿に関する関数
 */

class Class_Post{
	
	/*
	 * 投稿経験値計算
	 *  投稿された内に対する経験値を計算します。
	 *  ※アップロードされたファイルに関する経験値の追加は[controller/api.php]で行います。
	 * @param (string)
	 * @return (int)
	 */
	public static function calcExp($post){
		$exp = 10 + floor((mb_strlen($post) * 1.5));
		return $exp;
	}
	
	/*
	 * 拡張子別サムネイルファイル名
	 *  $this->postMake() にて、アップロードファイルを整形する際に拡張子に対するサムネイルを取得します。
	 * @param (string)
	 * @return (string)
	 */
	public  function extensionThumbnail($ext){
		if($ext == 'jpg' || $ext == 'png' || $ext == 'gif' || $ext == 'bmp'){
			return 'image';
		}elseif($ext == 'avi' || $ext == 'flv' || $ext == 'mlv' || $ext == 'mov' || $ext == 'webm' || $ext == 'wmv' || $ext == 'mpg' || $ext == 'mpeg'){
			return 'video';
		}elseif($ext == 'rar' || $ext == 'zip' || $ext == '7z' || $ext == 'gz' || $ext == 'tar.gz' || $ext == 'tar'){
			return 'compress';
		}elseif($ext == 'txt' || $ext == 'csv'){
			return 'text';
		}elseif($ext == 'mp3' || $ext == 'wav' || $ext == 'flac'){
			return 'music';
		}else{
			return $ext;
		}
	}
	
	/*
	 * 投稿整形
	 *  投稿された内容を整形します。
	 * @param (string)
	 * @return (string)
	 */
	public function postMake($post){
		$message = $post->message;
		$date = strtotime('Y/m/d*', $post->created_at);
		
		mb_regex_encoding("UTF-8");
		$key_1 = '';
		$key_2 = '';
		$first = [];
		$secound = [];
		$bold = [];
		$addres = [];
		$youtube = [];
		
		// リンク
		if(preg_match_all('/https?:\/\/[\w\/:%#\$&\?\(\)~\.=\+\-]+/iu', $message, $address)){
			//require_once 'simple_html_dom.php';
			foreach($address as $key_1 => $first){
				foreach($first as $key_2 => $secound){
					//mb_language('Japanese');
					//$html = file_get_html( $secound );
					//$title = $html->find("title", 0);
					//$descr = $html->find("meta['description']", 0);
					//file_put_contents('c:\kkk.txt', print_r($title));
					//exit();
					$message = str_replace($secound, '<a href="'.$secound.'" class="link" target="blank">'.$secound.'</a>', $message);
					//$html->clear();
				}
			}
		}
		
		// 装飾
		if(preg_match_all('/\[b\](.*?)\[\/b\]/su', $message, $bold)){
			foreach($bold as $key_1 => $first){
				foreach($first as $key_2 => $secound){
					$message = str_replace('[b]'.$secound.'[/b]', '<span class = "bold">'.$secound.'</span>', $message);
				}
			}
		}
		
		if(preg_match_all('/\[i\](.*?)\[\/i\]/su', $message, $bold)){
			foreach($bold as $key_1 => $first){
				foreach($first as $key_2 => $secound){
					$message = str_replace('[i]'.$secound.'[/i]', '<span class = "italic">'.$secound.'</span>', $message);
				}
			}
		}
		
		if(preg_match_all('/\[u\](.*?)\[\/u\]/su', $message, $bold)){
			foreach($bold as $key_1 => $first){
				foreach($first as $key_2 => $secound){
					$message = str_replace('[u]'.$secound.'[/u]', '<span class = "underline">'.$secound.'</span>', $message);
				}
			}
		}
		
		if(preg_match_all('/\[s\](.*?)\[\/s\]/su', $message, $bold)){
			foreach($bold as $key_1 => $first){
				foreach($first as $key_2 => $secound){
					$message = str_replace('[s]'.$secound.'[/s]', '<span class = "strike">'.$secound.'</span>', $message);
				}
			}
		}
		
		if(preg_match_all('/\[B\](.*?)\[\/B\]/su', $message, $bold)){
			foreach($bold as $key_1 => $first){
				foreach($first as $key_2 => $secound){
					$message = str_replace('[B]'.$secound.'[/B]', '<span class = "size_big">'.$secound.'</span>', $message);
				}
			}
		}
		
		if(preg_match_all('/\[S\](.*?)\[\/S\]/su', $message, $bold)){
			foreach($bold as $key_1 => $first){
				foreach($first as $key_2 => $secound){
					$message = str_replace('[S]'.$secound.'[/S]', '<span class = "size_small">'.$secound.'</span>', $message);
				}
			}
		}
		
		//コマンド機能
		if(preg_match('/!beluga/su', $message)){
			$message = str_replace('!beluga', "<a href='http://daizu.beluga.fm' class='link' target='blank'>daizu.beluga.fm</a>", $message);
		}
		
		if(preg_match('/!twister/su', $message)){
			$message = str_replace('!twister', "<a href='http://twister.kzho.net' class='link' target='blank'>twister.kzho.net</a>", $message);
		}
		
		if(preg_match('/!wadai/su', $message)){
			$message = str_replace('!twister', "<a href='http://news.yahoo.co.jp/' class='link' target='blank'>Yahoo!ニュース</a>", $message);
		}
		
		if(preg_match_all('/@([\d\w]*)/is', $message, $bold)){
			foreach($bold as $key_1 => $first){
				foreach($first as $key_2 => $secound){
					$message = str_replace('@'.$secound.'', '<a href="http://twister.kzho.net/user/'.$secound.'" class="link" target="blank">@'.$secound.'</a>', $message);
				}
			}
		}
		
		if(preg_match('/!kuzuho/su', $message)){
			$message = str_replace('!kuzuho', '<span class = "bold" style="color:red;">admin大好き</span>', $message);
		}
		
		if(preg_match('/!omikuji/su', $message)){
			$hash = sha1(date('Y/m/d*', $post->created_at) . $post->user_id);
			$hash = substr($hash, 0, 1);
			$resultArray = array(
				'a'=>'<span class="omikuji daikichi">大豆</span>', 
				'b'=>'<span class="omikuji daikichi">大吉</span>', 
				'c'=>'<span class="omikuji chukichi">中吉</span>', 
				'd'=>'<span class="omikuji chukichi">中吉</span>', 
				'e'=>'<span class="omikuji chukichi">中吉</span>', 
				'f'=>'<span class="omikuji shokichi">小吉</span>', 
				'g'=>'<span class="omikuji shokichi">小吉</span>', 
				'h'=>'<span class="omikuji shokichi">小吉</span>', 
				'i'=>'<span class="omikuji shokichi">小吉</span>', 
				'j'=>'<span class="omikuji kichi">吉</span>', 
				'k'=>'<span class="omikuji kichi">吉</span>', 
				'l'=>'<span class="omikuji kichi">吉</span>', 
				'm'=>'<span class="omikuji kichi">吉</span>', 
				'n'=>'<span class="omikuji kichi">吉</span>', 
				'o'=>'<span class="omikuji suekichi">末吉</span>', 
				'p'=>'<span class="omikuji suekichi">末吉</span>', 
				'q'=>'<span class="omikuji suekichi">末吉</span>', 
				'i'=>'<span class="omikuji suekichi">末吉</span>', 
				's'=>'<span class="omikuji suekichi">末吉</span>', 
				't'=>'<span class="omikuji suekichi">末吉</span>', 
				'u'=>'<span class="omikuji kyo">凶</span>', 
				'v'=>'<span class="omikuji kyo">凶</span>', 
				'w'=>'<span class="omikuji kyo">凶</span>', 
				'x'=>'<span class="omikuji kyo">凶</span>', 
				'y'=>'<span class="omikuji kyo">凶</span>', 
				'z'=>'<span class="omikuji kyo">凶</span>', 
				'0'=>'<span class="omikuji kyo">凶</span>', 
				'1'=>'<span class="omikuji daikyo">大凶</span>', 
				'2'=>'<span class="omikuji daikyo">大凶</span>', 
				'3'=>'<span class="omikuji daikyo">大凶</span>', 
				'4'=>'<span class="omikuji daikyo">大凶</span>', 
				'5'=>'<span class="omikuji daikyo">大凶</span>', 
				'6'=>'<span class="omikuji daikyo">大凶</span>', 
				'7'=>'<span class="omikuji daikyo">大凶</span>', 
				'8'=>'<span class="omikuji daikichi"><font color="#ff0000">★</font><font color="#ff001c">★</font><font color="#ff0038">★</font><font color="#ff0055">大</font><font color="#ff0071">★</font><font color="#ff008d">★</font><font color="#ff00aa">吉</font><font color="#ff00c6">★</font><font color="#ff00e2">★</font><font color="#ff00ff">★</font></span>', 
				'9'=>'<span class="omikuji daikichi"><a href="http://daizu.beluga.fm/" target="_blank"><font color="#ff0000">★</font><font color="#ff000e">★</font><font color="#ff001c">★</font><font color="#ff002a">★</font><font color="#ff0038">★</font><font color="#ff0046">★</font><font color="#ff0055">大</font><font color="#ff0063">★</font><font color="#ff0071">★</font><font color="#ff007f">★</font><font color="#ff008d">★</font><font color="#ff009b">★</font><font color="#ff00aa">吉</font><font color="#ff00b8">★</font><font color="#ff00c6">★</font><font color="#ff00d4">★</font><font color="#ff00e2">★</font><font color="#ff00f0">★</font><font color="#ff00ff">★</font></a></span>', 
			);
			$message = str_replace('!omikuji', $resultArray[$hash], $message);
		}
		
		//youtube
		/*
		if(preg_match_all('/https?:\/\/w?w?w?\.?youtube\.com\/watch\?v=([\w\d]{11})?/i', $message, $youtube)){
			foreach($youtube as $key_1 => $first){
				foreach($first as $key_2 => $secound){
					$message = str_replace(''.$secound.'', '<iframe style="text-align:center;width:100%;max-width:640px;height:360px;" src="https://www.youtube.com/embed/K9HjYTFoMKk" frameborder="0" allowfullscreen ></iframe>', $message);
				}
			}
		}
		*/
		
		//vimeo
		/*
		if(preg_match_all('/https?:\/\/w?w?w?\.?youtube\.com\/watch\?v=([\w\d]{11})?/i', $message, $youtube)){
			$youtube_key = reset($youtube);
			for($i = 0; $i < count($youtube_key); $i++){
				$message = str_replace($youtube[0][$i], '<div class="embed_video_container"><iframe src="http://player.vimeo.com/video/6284199?title=0&byline=0&portrait=0" frameborder="0"></iframe></div>', $message);
			}
		}
		*/
		
		//アップロードファイル整形
		$crop = explode("\n", $message);	
		
		foreach($crop as $key => $cropVal){
			if(preg_match_all('/\[F\](.*?)\[\/F\]/su', $cropVal, $file)){
				$crop[$key] .= '<div class="article_file">';
				foreach($file as $key_1 => $first){
					foreach($first as $key_2 => $secound){
						$file_method = Model_Upload::find('all',array(
							'related'=>array('user'),
							'where' => array(array('upload_id', $secound))
						));
						$file_array = reset($file_method);
						if(!isset($file_array['upload_id'])){continue;}
						//サムネイル
						if(strstr($file_array['upload_ext'],'jpg') OR strstr($file_array['upload_ext'],'png') OR strstr($file_array['upload_ext'],'jpeg') OR strstr($file_array['upload_ext'],'gif')){
							$file_size = '';
							$thumbnail = '<img src="http://data.kzho.net/up/thumbnail/'.$file_array['upload_id'].'.png">';
						}else{
							$file_size = ceil($file_array['upload_size'] / 1000 / 1000).' MB';
							if(file_exists('M:/twister/up/thumbnail/'.$file_array['upload_id'].'.png')){
								$thumbnail = '<img src="http://data.kzho.net/up/thumbnail/'.$file_array['upload_id'].'.png">';
							}else{
								$thumbnail = '<img src="http://data.kzho.net/up/icon/'.$this->extensionThumbnail($file_array['upload_ext']).'.png">';
							}
						}
						if($this->extensionThumbnail($file_array['upload_ext']) == 'image'){
							$description = '';
							$extension = '';
							$size = '';
						}else{
							$description = "投稿者: ".$file_array->user->user_name."&#10;ファイル名: ".$file_array->upload_name." ";
							$extension = '<p class="info extention">'.strtoupper($file_array['upload_ext']).'</p>';
							$size = '<p class="info size">'.$file_size.'</p>';
						}
						$crop[$key] .= '<div class="article_file_thumb"><p class="info preview"><b>'.$file_array['upload_count'].'</b> pv</p>'.$extension.$size.'<a href="http://twister.kzho.net/file/'.$file_array['upload_id'].'.'.$file_array['upload_ext'].'" target="_blank" title="'.$description.'">'.$thumbnail.'</a></div>';
						$crop[$key] = str_replace('[F]'.$secound.'[/F]', '', $crop[$key]);
						//$message = str_replace('[F]'.$secound.'[/F]', '', $message);
						//$message = str_replace('[f]'.$secound.'[/f]', '<div class="article_file"><a href="http://kzho.net/">123</div>', $message);
					}
				}
				$crop[$key] .= '</div>';
			}
		}
		
		//imgur
		foreach($crop as $key => $cropVal){
			if(preg_match_all('/https?:\/\/w?w?w?\.?i?\.?imgur\.com\/(([\w\d]{5,7})?\.(jpg|png|gif))/i', $message, $youtube)){
				//print_r($youtube);exit();
				$crop[$key] .= '<div class="article_file">';
				foreach($youtube[1] as $key_1 => $first){
					$crop[$key] = str_replace($youtube[0][$key_1], '', $crop[$key]);
					$thumbnail = '<img src="http://i.imgur.com/'.$youtube[2][$key_1].'b.'.$youtube[3][$key_1].'" style="width:108px;height:108px;">';
					$crop[$key] .= '<div class="article_file_thumb"><p class="info preview">imgur</p><a href="http://i.imgur.com/'.$first.'" target="_blank">'.$thumbnail.'</a></div>';
				}
				$crop[$key] .= '</div>';
			}
		}
		$message = implode($crop);		
		return $message;
	}
}











	
	/*
	public static function timeCourse($time){
		$diff = time() - $time;
		if($diff < 61){
			return $diff.'秒前';
		}elseif($diff < 3600){
			$return = ceil($diff / 60);
			return $return.'分前';
		}elseif($diff < 81600){
			$return = ceil($diff / 60 / 60);
			return $return.'時間前';
		}elseif($diff < 29784000){
			$return = ceil($diff / 60 / 60 / 60);
			return $return.'日前';
		}
	}
	
	public static function dateMake($time){
		$diff = strtotime(date('Y/m/d 0:00:00')) - $time;
		if($diff < 0){
			return date('H:i', $time);
		}else{
			return date('m/d H:i', $time);
		}
	}
	*/