<?php

/*
 * ルーム
 */

class Class_Room{

	/*
	 * ルームID検索
	 * @return (object||bool)
	 */
	public static function find_room_id($room_name){
		$model = Model_Room::find('all', array(
			'where' => array(
				array('room_name' => $room_name)
			)
		));
		$model = reset($model);
		return (is_object($model)) ? $model->id : false;
	}
}