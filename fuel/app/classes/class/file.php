<?php

/*
 * ファイルクラス
 *  ファイル全般の処理を扱います。
 */

class Class_File{
	public $user_id;
	public $tmep;
	public $mode;
	public $file = [];
	
	
	/*
	 * 準備
	 * @return void
	 */
	private function userStandBy(){
		if(!file_exists('M:/twister/icon/user/'.$this->user_id.'/')){
			mkdir('M:/twister/icon/user/'.$this->user_id.'/');
		}
	}
	
	/*
	 * ユーザーアイコン処理
	 * @return void
	 */
	public function userIcon(){
		$this->userStandBy();
		exec("M:/nconvert/nconvert.exe -out png -truecolors -keepfiledate -resize 50 50 -overwrite -o M:/twister/icon/user/".$this->user_id."/50x.png ".$this->temp);
		exec("M:/nconvert/nconvert.exe -out png -truecolors -keepfiledate -resize 64 64 -overwrite -o M:/twister/icon/user/".$this->user_id."/64x.png ".$this->temp);
		exec("M:/nconvert/nconvert.exe -out png -truecolors -keepfiledate -resize 100 100 -overwrite -o M:/twister/icon/user/".$this->user_id."/100x.png ".$this->temp);
		exec("M:/nconvert/nconvert.exe -out png -truecolors -keepfiledate -overwrite -o M:/twister/icon/user/".$this->user_id."/original.png ".$this->temp);
	}
	
	/*
	 * ユーザー背景画像処理
	 * @return void
	 */
	public function userBackground(){
		$this->userStandBy();
		exec("M:/nconvert/nconvert.exe -out jpeg -q 100 -truecolors -keepfiledate -overwrite -o M:/twister/icon/user/".$this->user_id."/back.jpg ".$this->temp);
	}
	
	/*
	 * 投稿画像 サムネイル生成
	 * @return void
	 */
	public function uploadImageThumbnail(){
		exec("M:/nconvert/nconvert.exe -out png -truecolors -keepfiledate -ratio -resize 192 108 -overwrite -o M:/twister/up/thumbnail/".$this->file['id'].".png".' M:/twister/up/sorce/"'.$this->file['id'].".".$this->file['extension']);
	}
	
	/*
	 * 投稿動画 サムネイル生成
	 * @return void
	 */
	public function uploadVideoThumbnail(){
		exec("ffmpeg -i M:/twister/up/sorce/".$this->file['id'].".".$this->file['extension']." -f image2 -vframes 1 -ss 5 -aspect 16:9 -s 108x108 -an -deinterlace M:/twister/up/thumbnail/".$this->file['id'].".png");
		//print_r("ffmpeg -i M:/twister/up/sorce/".$this->file['id'].".".$this->file['extension']." -f image2 -vframes 1 -ss 3 -s 100x100 -an -deinterlace M:/twister/up/thumbnail/".$this->file['id'].".png");
	}
}