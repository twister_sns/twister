<?php

/*
 * クエリ
 *  様々な条件に沿った投稿をFuelPHPのクエリクラスを使って取得します。
 *  findRoomListではルーム情報を取得します。
 */

class Class_Query{
	public $control;
	public $select;
	public $mode;
	public $page;

	public $offset;
	public $limit;

	public $cRoom;

	public function __construct() {
		$this->cRoom = new Class_Room();
	}

	/*
	 * クエリオーダー
	 *  取得内容を$this->controlに沿って自動で分岐します。
	 *
	 * @return (array)
	 */
	public function queryOrder(){
		//offset, limitを計算
		$this->offset = $this->page * 25 - 25;
		$this->limit = $this->page * 25;

		if($this->control == 'room' || $this->control == 'main'){
			return $this->findRoom(); // ルーム投稿取得
		}elseif($this->control == 'roomList'){
			return $this->findRoomList(); // ルームリスト取得
		}elseif($this->control == 'public'){
			return $this->findPublic(); // パブリック投稿取得
		}elseif($this->control == 'user'){
			return $this->findUser(); // ユーザー投稿取得
		}elseif($this->control == 'single' OR $this->control == 'post'){
			return $this->findPostSingle(); // 単一投稿取得
		}
	}

	/*
	 * 単体投稿取得
	 *  指定したIDの投稿を取得します。
	 * @return posts (object)
	 * @return rows (int)
	 */
	public function findPostSingle(){
		$posts = Model_Post::find('all', array(
			'where' => array(
				array('id', $this->select),
				array('del', 0)
			)
		));
		$post = reset($posts);
		$row = (is_object($post))?1:0;

		return array('posts'=>$posts,'rows'=>$row);
		//$post = (is_object($post))?$post:false;
	}

	/*
	 * ルーム検索
	 *  現時点でページング非対応です
	 * @return return (array)
	 * @return rows (int)
	 */
	protected function findRoomList(){
		// 検索結果件数取得
		if($this->select){
			// room/category/
			$room_rows = Model_Room::query()
						->where('room_category',$this->select)
						->where('room_hidden', 0)
						->where('room_del', 0)
						->where('id','!=',1)
						->count();
			$querySelect = array('room_category',$this->select);
		}else{
			// room/
			$room_rows = Model_Room::query()
						->where('id','!=',1)
						->where('room_hidden', 0)
						->where('room_del', 0)
						->count();
			$querySelect = array('room_category','!=',0);
		}

		// 検索結果取得
		$rooms = Model_Room::find('all', array(
			'related' => array(
				'post' => array(
					'where' => array(
						array('del' => 0),
					),
					'order_by' => array(
						'created_at' => 'desc'
					),
				),
			),
			'where' => array(
				array('id', '!=', 1),
				array('room_hidden', 0),
				array('room_del', 0),
				$querySelect
			),
			'rows_limit' => 80,
		));
		return array('return'=>$rooms, 'rows'=>$room_rows);
	}

	/*
	 * ルーム情報取得
	 *  指定したIDのルーム情報を取得します。
	 * @return posts (array)
	 * @return rows (int)
	 */
	protected function findRoom(){
		if($this->control == 'room'){
			$this->select = $this->cRoom->find_room_id($this->select); // ルームID取得
		}

		$rows = Model_Post::query()
			->where('room_id', $this->select)
			->where('del', 0)
			->count();

		$posts = Model_Post::find('all', array(
			'where' => array(
				array('room_id', $this->select),
				array('del', 0),
			),
			'order_by' => array('id' => 'desc'),
			'limit' => $this->limit
		));

		return array('posts'=>$posts, 'rows'=>$rows);
	}

	/*
	 * パブリック取得
	 *  現時点でページング非対応です
	 * @return posts (array)
	 * @return rows (rows)
	 */
	protected function findPublic(){

		$rows = Model_Post::query()
			->related('room',array(
				'where' => array(
					array('room_hidden', '=', 0)
					)
				)
			)
			->where('room_id', $this->select)
			->where('del', 0)
			->count();

		$posts = Model_Post::find('all', array(
			'related' => array(
				'room' => array(
					'where' => array(
						array('room_hidden', 0)
					),
				),
			),
			'where' => array(
				array('del', 0)
			),
			'order_by' => array(
				array('id', 'desc')
			),
			'rows_limit' => $this->limit
		));

		return array('posts'=>$posts, 'rows'=>$rows);
	}

	/*
	 * ユーザー投稿取得
	 *  初期クエリへモード別処理を追加したモデルを処理します。
	 *  現時点で件数取得に対応していません。
	 * @return posts (array)
	 * @return rows (NULL)
	 */
	protected function findUser(){
		$id = $this->find_id($this->select);
		$mode = [];

		//初期クエリ
		$query = array(
			'related' => array(
				'room' => array(
					'where' => array(
						array('room_hidden', 0)
					)
				),
			),
			'where' => array(
				array('del', 0),
			),
			'order_by' => array('id' => 'desc'),
			'rows_limit' => $this->limit
		);

		//モード別処理
		switch ($this->mode){
			case 'post':
				$query['where'][] = array('room_id', '!=', 0);
				$query['where'][] = array('user_id', $id);
				break;
			case 'room_post':
				$query['where'][] = array('room_id', '!=', 1);
				$query['where'][] = array('user_id', $id);
				break;
			case 'was_fav':
				$query['related']['favorite'] = [];
				$query['related']['favorite']['where'][] = array('user_id', $id);
				break;
			case 'has_fav':
				$query['related']['favorite'] = [];
				$query['related']['favorite']['where'][] = array('fav_id', $id);
				break;
			case 'was_good':
				$query['related']['good'] = [];
				$query['related']['good']['where'][] = array('user_id', $id);
				break;
			case 'has_good':
				$query['related']['good'] = [];
				$query['related']['good']['where'][] = array('good_id', $id);
				break;
			case 'mute':
				$return = $this->findUserMute();
				return $return;
		}

		$posts = Model_Post::find('all', $query);
		return array('posts'=>$posts, 'rows'=>null);
	}

	/*
	 * ミュートユーザー情報取得
	 *  指定したIDのミュートユーザーを取得します。
	 * @return (array)
	 */
	public function findUserMute(){
		$mutes = Model_Mute::find('all', array(
			'where' => array(
				array('user_id', $this->id),
			),
			//'order_by' => array('id' => 'desc'),
			//'limit' => $this->limit
		));
		echo DB::last_query();
		return $mutes;
	}

	/*
	 * ユーザーnum検索
	 *  ユーザーIDを元にユーザーnumを取得します。
	 *  $this->findUser()のみで使用しています。
	 * @return (int)
	 */
	public  function find_id($user_id){
		$user = Model_User::find('all',array(
			'where' => array(
				array('user_id', $user_id)
			)
		));
		$user = reset($user);
		return $user->id;
	}
}