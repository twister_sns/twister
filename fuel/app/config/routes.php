<?php
return array(
	'_root_'	=> 'top/index',				// Twister トップ
	'_404_'		=> 'welcome/404',			// 404
	
	'file/(:any)' => 'file/index/$1',		// アップロードファイル
	'room/(:segment)' => 'room/index/$1',	// ルーム
	'user/(:any)' => 'user/index/$1',		// ユーザーページ
	'post/(:num)' => 'post/index/$1',		// 指定投稿
	
	'logout' => 'top/logout',				// ログアウト
	'send' => 'top/send',					// ログイン情報
);
