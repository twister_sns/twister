<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title><?if(isset($title)){echo $title.' / Twister';}else{echo 'Twister';}?></title>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	
	<?=ltrim(Asset::js('dropzone.js'))?>
	<?=ltrim(Asset::js('jquery.inview.js'))?>
	<?=ltrim(Asset::js('jquery.selection.js'))?>
	<?=ltrim(Asset::js('jquery.fittext.js'))?>
	<?=ltrim(Asset::js('slidebars.js'))?>
	<?=ltrim(Asset::js('script.js'))?>
	
	<?=ltrim(Asset::css('dropzone.css'))?>
	<?=ltrim(Asset::css('toggle-switch.css'))?>
	<?=ltrim(Asset::css('slidebars.min.css'))?>
	<?=ltrim(Asset::css('bootcust.css'))?>
	<?=ltrim(Asset::css('quick.css'))?>
	<?=ltrim(Asset::css('style.css'))?>
	
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script>
		<? /* 変数宣言 */ ?>
		var returnTitle = 0;
		var returnCount = 0;
		var control = '<?=Uri::segment(1)?>';
		var select = <?if(Uri::segment(2)):?>'<?=Uri::segment(2)?>'<?else:?>''<?endif;?>;
		var mode = <?if(Uri::segment(3)):?>'<?=Uri::segment(3)?>'<?else:?>''<?endif;?>;
		
		<? /* ユーザー情報 */ ?>
		<? if(isset($postUser)):?>
			var id = <?if(isset($postUser['id'])):?><?=$postUser['id']?><?else:?>null<?endif;?>;
			var user_id = <?if($postUser['user_id']):?>'<?=$postUser['user_id']?>'<?else:?>null<?endif;?>;
		<? else: ?>
			var id = <?if(isset($user['id'])):?><?=$user['id']?><?else:?>null<?endif;?>;
			var user_id = <?if($user['user_id']):?>'<?=$user['user_id']?>'<?else:?>null<?endif;?>;
		<? endif;?>
					
		<? /* 関数実行 */ ?>
		getUserStatus(id);
	</script>
	
	<? /* 背景画像が個別に用意されている場合 */ ?>
	<? if(file_exists('M:/twister/icon/user/'.$user['id'].'/back.jpg')): ?>
		<style>
			body{
				background-image:url('http://data.kzho.net/icon/user/<?=$user['id']?>/back.jpg');
				background-size:cover;
				background-color:#FFFFFF;
			}
		</style>
	<? endif; ?>
		
	<? /* ミュートユーザーの非表示 */?>
	<? if(is_array($mute_users)): ?>
		<style>
			<? foreach($mute_users as $value): ?>
			.user-id-<?=$value->block_id?>{
				display:none;
			}
			<? endforeach; ?>
		</style>
	<? endif;?>
</head>
<body>
	<? // 768以下:xs, 992未満:sm, 1200未満:md, 1200以上:lg ?>
	
	<!-- *ナビゲーション* -->
	<?=$navbar;?>
		
	<!-- *アラート* -->
	<!-- *jquery animateに治す* -->
	<div class="alert twisterAlert" role="alert" id='twisterAlert'></div>
			
	<!-- *カラム判別* -->
	<?
		//メイン前処理
		if(!isset($right)){$right = false;}
		if(!isset($left)){$left = false;}
	?>
	
	<?/* ●トップ● */?>
	<? if(isset($top)): ?>
		<div style ="margin-top:50px;margin-bottom:25px;">
			<?=$top?>
		</div>
	<? else: ?>
		<div style="margin-top:50px;"></div>
	<? endif; ?>
	
	<section class="container sb-close" style="">
		<div class="row">
			
			<?/* ●センター● */?>
			<? if($right == false && $left == false): ?>
				<div class="col-md-12 center" style='padding:0 5px;'>
					<?=ltrim($content);?>
				</div>
			<? elseif($right == false || $left == false): ?>
				<div class="col-md-9 col-md-push-3 center" style='padding:0 5px;'>
					<?=ltrim($content);?>
				</div>
			<? else: ?>
				<div class="col-md-6 col-md-push-3 center" style='padding:0 5px;'>
					<?=ltrim($content);?>
				</div>
			<? endif; ?>
			
			<?/* ●左サイド● */?>
			<? if($left != false AND $right == false): ?>
				<div class="col-md-3 col-md-pull-9 left" style='padding:0 5px;'>
					<?=$left;?>
				</div>
			<? elseif($left != false AND $right != false): ?>
				<div class="col-md-3 col-md-pull-6 left" style='padding:0 5px;'>
					<?=$left;?>
				</div>
			<? endif; ?>
			
			<?/* ●右サイド● */?>
			<? if($right != false): ?>
			<div class="col-md-3 right" style="">
				<?=ltrim($right);?>
			</div>
			<? endif;?>
		</div>
	</section>
</body>
</html>