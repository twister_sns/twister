<? if($navbar_mode == 'user'): ?>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<!-- スマートフォンサイズで表示されるメニューボタンとテキスト -->
			<div class="navbar-header">
				<!--
					メニューボタン
					data-toggle : ボタンを押したときにNavbarを開かせるために必要
					data-target : 複数navbarを作成する場合、ボタンとナビを紐づけるために必要
				-->
				<!-- 各ナビゲーションメニュー -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-menu-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<?=Html::anchor('/', 'Twister', array('class'=>'navbar-brand'))?>
			</div>
			
			<div class="collapse navbar-collapse" id="nav-menu-1">
				<ul class="nav navbar-nav navbar-left">
					<? if(strstr(Uri::current(), 'main')):?><li class="active"><? else: ?><li><? endif;?>
					<?=Html::anchor('main', 'ホーム')?></li>
					<li><a href="#">通知</a></li>
					<? if(strstr(Uri::current(), 'room')):?><li class="active"><? else: ?><li><? endif;?>
					<?=Html::anchor('room', 'ルーム')?></li>
					<? if(strstr(Uri::current(), 'public')):?><li class="active"><? else: ?><li><? endif;?>
					<?=Html::anchor('public', 'パブリック')?></li>
					<li><a href="#">メッセージ</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">お気に入り <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li class="divider"></li>
							<li><a href="#">Separated link</a></li>
							<li class="divider"></li>
							<li><a href="#">One more separated link</a></li>
						</ul>
					</li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">アカウント<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="#">マイページ</a></li>
							<li><a href="#">プロフィール</a></li>
							<li><?=Html::anchor('setting', '設定')?></li>
							<li class="divider"></li>
							<li><a href="#">ファイル管理</a></li>
							<li class="divider"></li>
							<li><?=Html::anchor('logout', 'ログアウト')?></li>
						</ul>
					</li>
				</ul>
				
			</div>
		</div>
	</nav>
<? else: ?>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<!-- スマートフォンサイズで表示されるメニューボタンとテキスト -->
			<div class="navbar-header">
				<!--
					メニューボタン
					data-toggle : ボタンを押したときにNavbarを開かせるために必要
					data-target : 複数navbarを作成する場合、ボタンとナビを紐づけるために必要
				-->
				<!--
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-menu-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				-->
				<!-- タイトルなどのテキスト -->
				<button type="button" class="btn btn-primary navbar-btn" onclick="location.href='http://twister.kzho.net/'">新規登録 / ログイン</button>
				<?=Html::anchor('/', 'Twister', array('class'=>'navbar-brand'))?>
			</div>
		</div>
	</nav>
<? endif; ?>