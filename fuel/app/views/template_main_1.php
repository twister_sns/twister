<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>twister</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
	<?=Asset::css('bootcust.css');?>
	<?=Asset::css('quick.css');?>
	<?=Asset::css('style.css');?>
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<!-- *ナビゲーション* -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<!-- スマートフォンサイズで表示されるメニューボタンとテキスト -->
			<div class="navbar-header">
				<!--
					メニューボタン
					data-toggle : ボタンを押したときにNavbarを開かせるために必要
					data-target : 複数navbarを作成する場合、ボタンとナビを紐づけるために必要
				-->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-menu-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!-- タイトルなどのテキスト -->
				<a class="navbar-brand" href="#">twistar</a>
			</div>
			<!-- グローバルナビの中身 -->
			<div class="collapse navbar-collapse" id="nav-menu-1">
			
				
				<!-- 各ナビゲーションメニュー -->
				<ul class="nav navbar-nav navbar-left">
					<!-- 通常のリンク -->
					<li class="active"><a href="#">ホーム</a></li>
					<li><a href="#">通知</a></li>
					<li><a href="#">ひとりごと</a></li>
					<li><a href="#">パブリック</a></li>
					<li><a href="#">Hot</a></li>
					<li><a href="#">メッセージ</a></li>
					<!-- ドロップダウンのメニューも配置可能 -->
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">お気に入り <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li class="divider"></li>
							<li><a href="#">Separated link</a></li>
							<li class="divider"></li>
							<li><a href="#">One more separated link</a></li>
						</ul>
					</li>
				</ul>
				
				
				<!-- 各ナビゲーションメニュー -->
				<ul class="nav navbar-nav navbar-right">
					<!-- 通常のリンク -->
					<li class="active"><a href="#">アカウント</a></li>
					<li><a href="#">アカウント</a></li>
					<!-- ドロップダウンのメニューも配置可能 -->
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">アカウント <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="#">マイページ</a></li>
							<li><a href="#">プロフィール</a></li>
							<li><a href="#">設定</a></li>
							<li class="divider"></li>
							<li><a href="#">ファイル管理</a></li>
							<li class="divider"></li>
							<li><a href="#">ログアウト</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	
	
	<!-- *アラート* -->
	<!--
	<div class="alert alert-success" role="alert">通知</div>
	-->
	
	<!-- *グリッド* -->
	<section class="container">
		<div class="row">
			<!-- 中部 -->
			
			<?
				/*
				** 768以下	xs
				** 992未満	sm
				** 1200未満	md
				** 1200以上	lg
				*/
			?>
			
			<?/* ●センター● */?>
			<div class="col-md-9 col-md-push-3" style="min-height:300px;">
			<div class="MAIN">			
			
			<div class="roomInformation">
				<div class="roomTitle">
					<h1 class="title">テスト</h1>
				</div>
				<div class="roomDescription">
					<p class="desc">テストルーム</p>
				</div>
			</div>
			
			<div class="postWrapper">
				<div class="postFace">
			    		<img src="http://account.beluga.fm/images/profile/10678309/x50.gif" class="postImg">
				</div>
				<div class="postBody">
					<div class="postContent">
						<div class="postInfo">
							<div class="postUser">
								<a href="http://twit.beluga.fm/home"><span class="user_name">クズホ</span><span class="user">@kuzuho</span></a>
							</div>
							<div class="postDate">
								<a href="http://twit.beluga.fm/home"><span class="date">4分前</span></a>
							</div>
						</div>
						<div class="clearBoth"></div>
						<div class="postArticle">
				    		投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分<br>
				    		投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分<br>
				    		投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分<br>
				    		投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分<br>
				    		投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分投稿部分<br>
			    		</div>
			    		<div class="postAction">
			    			<span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span>
			    		</div>
			    	</div>
				</div>
			</div>
			</div>
			
			</div>
			<?/* ●左サイド● */?>
			<div class="col-md-3 col-md-pull-9" style="background-color:#000000;min-height:300px;">
				
			</div>
		</div>
	</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>