<div class='LEFT'>
	<div class='sidebar_top'>
		<?=Html::anchor('room', 'ルーム トップ')?>
	</div>
	
	
	<div class='sidebar_menu'>
		<div class="menu_title">カテゴリ</div>
		<ul class='menu_group'>
			<? foreach($room_categories as $key => $category): ?>
				<li class="menu_link"><?=Html::anchor('room/category/'.$category->id, $category->category, array())?></li>
			<? endforeach; ?>
		</ul>
	</div>
</div>