<div class='LEFT'>
	<div class="profile_box">
		<div class="name">
			<? if(isset($postUser)): ?>
				<p class="user_name"><?=Html::anchor('user/'.$postUser['user_id'], $postUser['user_name'])?></p>
				<p class="user_id"><?=Html::anchor('user/'.$postUser['user_id'], '@'.$postUser['user_id'])?></p>
			<? else: ?>
				<p class="user_name"><?=Html::anchor('user/'.$user['user_id'], $user['user_name'])?></p>
				<p class="user_id"><?=Html::anchor('user/'.$user['user_id'], '@'.$user['user_id'])?></p>
			<? endif;?>
		</div>
		<div class="face">
			<? if(isset($postUser)): ?>
				<?=Html::anchor('user/'.$postUser['user_id'], '<img src="http://data.kzho.net/icon/user/'.$postUser['id'].'/64x.png" style="width:62px;height:62px;">')?>
			<? else: ?>
				<?=Html::anchor('user/'.$user['user_id'], '<img src="http://data.kzho.net/icon/user/'.$user['id'].'/64x.png" style="width:62px;height:62px;">')?>
			<? endif;?>
		</div>
		<div class="status">
			<p class="profile_box_user_post int profile_box_status" id="">取得中…</p>
			<p class="desc">投稿</p>
		</div>
		<div class="status">
			<p class="profile_box_user_level int profile_box_status" id="">取得中…</p>
			<p class="desc">Lv</p>
		</div>
		<div class="status">
			<p class="profile_box_user_exp int profile_box_status" id="">取得中…</p>
			<p class="desc">Exp</p>
		</div>
		<div class="clearBoth"></div>
	</div>

	<script type="text/javascript">
		$(".profile_box_status").fitText(0.6, { minFontSize: '9px', maxFontSize: '24px' });
	</script>

 	<div class="sidebar_menu">
		<div class="menu_title">
			開発状況
		</div>
		<div class='message_field'>
			<p><a target="_blank" href="https://bitbucket.org/twister_sns/twister" style="font-weight:bold;font-size:19px;color:green;">オープンソースです(Git)</a></p>
			<ul>バグその他
				<li><b>投稿整形部分作り直し</b></li>
			</ul>
			<ul>作っている物
				<li>アップロードしたファイルの一覧表示、ファイル引用</li>
				<li>投稿前の画像をクリック</li>
				<li>スマホ用デザイン</li>
			</ul>
			<ul>計画中のもの
				<li>ID制</li>
				<li>ルームのフォローファボ</li>
				<li>Youtube/ニコニコ動画などの埋込</li>
				<li><font color=gray>投稿関連機能･!</font></li>
			</ul>
			<ul>作りたい物
				<li>@関連</li>
				<li>スマホ表示時にサイドバーの内容を表示するボタン</li>
				<li>スマホ表示時にTOPへ戻るボタン</li>
			</ul>
			<ul>◆アップロードの仕様 7/3
				<li>1ファイル上限200MB</li>
				<li>20個まで一度に選択出来ます</li>
				<li>画像<br>jpg,png,gif,bmp</li>
				<li>動画<br>mp4,avi,wmv,flv,mkv,mov,webm,mpg,mpeg</li>
				<li>音声<br>mp3,wav,flac</li>
				<li>圧縮<br>zip,rar,7z,gz,tar.gz,tar</li>
				<li>その他<br>txt,swf,pdf,xlsx,docx,csv</li>
			</ul>
		</div>
	</div>
</div>