<? if($user_block == false): ?>

<div class='sidebar_menu'>
	<div class="menu_title">ユーザー</div>
	<ul class='menu_group'>
		<li class="menu_link <?if(Uri::segment(3) == 'post'):?>active<?endif;?>"><?=Html::anchor('user/'.$user->user_id.'/post', 'すべての投稿')?></li>
		<li class="menu_link <?if(Uri::segment(3) == 'room_post'):?>active<?endif;?>"><?=Html::anchor('user/'.$user->user_id.'/room_post', 'ルームの投稿')?></li>
		<li class="menu_link <?if(Uri::segment(3) == 'was_fav'):?>active<?endif;?>"><?=Html::anchor('user/'.$user->user_id.'/was_fav', 'お気に入りの投稿')?></li>
		<li class="menu_link <?if(Uri::segment(3) == 'has_fav'):?>active<?endif;?>"><?=Html::anchor('user/'.$user->user_id.'/has_fav', 'お気に入りされた投稿')?></li>
		<li class="menu_link <?if(Uri::segment(3) == 'was_good'):?>active<?endif;?>"><?=Html::anchor('user/'.$user->user_id.'/was_good', 'goodした投稿')?></li>
		<li class="menu_link <?if(Uri::segment(3) == 'has_good'):?>active<?endif;?>"><?=Html::anchor('user/'.$user->user_id.'/has_good', 'goodされた投稿')?></li>
		<?/*<li class="menu_link <?if(Uri::segment(3) == 'mute'):?>active<?endif;?>"><?=Html::anchor('user/'.$user->user_id.'/mute', 'ミュート ユーザー')?></li>*/?>
	</ul>
</div>

<? endif; ?>