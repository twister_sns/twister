
<style>
	.user_profile{
		background-image:url('http://data.kzho.net/icon/user/<?=$user->id?>/background.png');
		background-repeat:no-repeat;
		background-position:center center;
		/*background-size:cover;*/
		/*background-attachment:fixed;*/
	}
</style>

<div class="user_profile">
	<div class="user_top">
			<div class="user_icon">
				<div class="user_icon_pic_wrapper">
					<?=Html::anchor('user/'.$user->user_id, '<img src="http://data.kzho.net/icon/user/'.$user->id.'/100x.png">')?>
				</div>
			</div>
				<div class="user_info">
					<h1 class="name">
						<?=Html::anchor('user/'.$user->user_id,$user->user_name)?>
					</h1>
					<h2 class="desc">
						<?=Html::anchor('user/'.$user->user_id,'@'.$user->user_id)?>
					</h2>
					<hr>
					<h3 class="desc">
						<? $user_block_mes = 'ブロックする'; ?>
						<? if(isset($mute_users)): ?>
							<? foreach($mute_users as $value): ?>
								<? //print_r($value); ?>
								<? if($value->block_id == $user->id): ?>
									<p class='user_block'>★このユーザーをミュートしています</p>
									<? $user_block_mes = 'ブロックを解除する';$user_block = true; ?>
								<? endif; ?>
							<? endforeach; ?>
						<? endif; ?>
						<?=nl2br($user->user_desc)?>
					</h3>
				</div>
		
					<div class="user_more">
						<a class="" href="#" id="" data-toggle="modal" data-target="#user_more"><span class="glyphicon glyphicon-cog" style="font-size:18px;"></span></a>
					</div>
	</div>
</div>


<!-- 設定モダル -->
<div class="modal fade" id="user_more" tabindex="-1" role="dialog" aria-labelledby="roomEditLabel" aria-hidden="true">
	<?//=Form::open(array('action'=>'api/room_edit.json', 'id'=>'form_room_edit'))?>
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="roomEditLabel">ユーザー設定</h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-info modal_action_alert" role="alert">
						<span class="modal_action_text"></span>
						<button type="button" data-dismiss="alert" class="close">
							<span>×</span>
						</button>
					</div>
					<div class='row'>
						<div class='col-lg-12' style='text-align:left;'>
							<?//=Form::label('ブロック', 'user_block')?>
							<?=Form::button('block-'.$user->id, 'このユーザーの'.$user_block_mes, array('class'=>'btn btn-danger btn-block', 'id'=>'user_action'))?>
							<!--<button type="button" class="btn btn-danger btn-block">ブロックする</button>-->
						<br>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
					<?//=Form::button('submit', '編集確定', array('class'=>'btn btn-primary'))?>
				</div>
			</div>
		</div>
	<?//=Form::close();?>
</div>