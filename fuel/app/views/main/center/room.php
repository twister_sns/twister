<script>
	var request = [];
	var returnFirst;
	
	
	getReadPost(false,1);
	setInterval(function(){getReadPost();},8000);
</script>

<div class="MAIN">			
	<div class="roomInformation">
		<div class="pull-left">
			<div class="roomTitle">
				<h1 class="title">
					<? if(!strstr(Uri::current(), 'public')): ?>
						<?=$room->room_name?>
					<? else: ?>
						パブリック
					<? endif;?>
				</h1>
			</div>
			<div class="room_description">
				<p class="desc">
					<? if(!strstr(Uri::current(), 'public')): ?>
						<?=nl2br($room->room_desc)?>
					<? else: ?>
						全ルームの投稿をチェック！
					<? endif;?>
				</p>
				<p class="room_create">
					作成日時：<?=nl2br(date('Y/m/d H:i:s',$room->created_at))?>
				</p>
			</div>
		</div>
		<div class='pull-right' style='text-align:right;'>
			<a class="btn btn-default" href="#" role="button" id="room_desc_open"><span class="glyphicon glyphicon-chevron-down"></span></a>
			<? if(!strstr(Uri::current(), 'main') && isset($room) && $room->user_id == Auth::get_uid()): ?>
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#roomEdit">ルーム編集</button><br>
			<? endif; ?>
			<? if(!strstr(Uri::current(), 'main') && !strstr(Uri::current(), 'public') && (!(isset($room) && $room->user_id == Auth::get_uid()))): ?>
				<?=$room->category->category?>
			<? endif; ?>
		</div>
		<div class='clearBoth'></div>
		
	</div>
	<div class='post_area'>
		<div class="formWrapper">
			<div class="face">
					<img src='http://data.kzho.net/icon/user/<?php if(isset($user['id'])){echo $user['id']; }else{echo 0;} ?>/50x.png' class='postImg'>
			</div>
			<div class="body">
					<?=Form::open(array('action'=>'main', 'enctype'=>'multipart/form-data', 'id'=>'form_room_post'))?>
						<?=Form::input('room_id', $room->id, array('type'=>'hidden'))?>
						<?=Form::input('add_files', '', array('type'=>'hidden', 'class'=>'add_files'))?>
						<?=Form::textarea('message', '', array('class'=>'form-control decontrol', 'rows'=>'3', 'placeholder'=>'クリック', 'minlength'=>'1', 'maxlength'=>'1000', 'id'=>'form_message'))?>
						<div class='decontrol' style='display:none;' id='form_message_decolation'>
							<li class='decoration'><div class='decorationWrapper'><a href='javascript::void()' id='decolation_bold'><i class="fa fa-bold fa-lg"></i></a></div></li
							><li class='decoration'><div class='decorationWrapper'><a href='javascript::void()' id='decolation_italic'><i class="fa fa-italic fa-lg"></i></a></div></li
							><li class='decoration'><div class='decorationWrapper'><a href='javascript::void()' id='decolation_underline'><i class="fa fa-underline fa-lg"></i></span></a></div></li
							><li class='decoration'><div class='decorationWrapper'><a href='javascript::void()' id='decolation_strike'><i class="fa fa-strikethrough fa-lg"></i></a></div></li
							><li class='decoration'><div class='decorationWrapper'><a href='javascript::void()' id='decolation_size_big'><i class="fa fa-font fa-lg"></i></a></div></li
							><li class='decoration'><div class='decorationWrapper'><a href='javascript::void()' id='decolation_size_small'><i class="fa fa-font"></i></a></div></li>
						</div>
						<div class="postAction">
							<table width="100%">
								<tbody>
									<tr>
										<td width="70%">
											<div class='dropzone' id="fileUpload" style=''></div>
										</td>
										<td width="30%" style="padding-left:15px;">
											<?=Form::button('submit', '投稿', array('class'=>'btn btn-default btn-block', 'id'=>'room_post_submit', 'style'=>'background-color:black;color:white;border:1px solid #000000;'))?>
										</td>
									</tr>
								</tbody>
							</table>
							<div id='fileUploadResult' class='dropzone-previews'>
							</div>
						</div>
					<?=Form::close()?>
			</div>
			<div class="clearBoth"></div>
		</div>
	
	<div id='room_post_out'>
		<div class="read_status">
			<div class="loading">
				<div class="progress">
					<div class="progress-bar progress-bar-striped active" aria-valuenow="100">読込中…</div>
				</div>
			</div>
			<div class="no_result">
				<p>表示する結果はありません。</p>
			</div>
		</div>
	</div>
	<div class='nextpage'></div>
	</div>
</div>

<? if(!strstr(Uri::current(), 'public')): ?>
<!-- ルーム編集モダル -->
<div class="modal fade" id="roomEdit" tabindex="-1" role="dialog" aria-labelledby="roomEditLabel" aria-hidden="true">
	<?=Form::open(array('action'=>'api/room_edit.json', 'id'=>'form_room_edit'))?>
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="roomEditLabel">ルーム編集</h4>
				</div>
				<div class="modal-body">
					<?=Form::hidden('mode', 'edit')?>
					<?=Form::hidden('room_id', $room->id)?>
					<div class='row'>
						<div class='col-lg-12' style='text-align:left;'>
						<?=Form::label('ルーム名前', 'room_name')?>
						<?=Form::input('room_name', $room->room_name, array('class'=>'form-control', 'id'=>'TCLIUserName', 'placeholder'=>'～64文字'))?>
						<br>
						<?=Form::label('ルーム説明', 'room_desc')?>
						<?=Form::textarea('room_desc', $room->room_desc, array('class'=>'form-control', 'rows'=>'3', 'placeholder'=>'～1024文字', 'id'=>'form_room_desc'))?>
						<br>
						<?=Form::label('ルームカテゴリ', 'room_category')?><br>
						<select name="form_room_category" id="form_room_category" class='form-control'>
							<? foreach($room_categories as $key => $category): ?>
								<? if($category->id == 1){continue;} ?>
								<option value="<?=$category->id?>" style="text-indent: 0px;" <? if($room->room_category == $category->id): ?>selected<? endif; ?>><?=$category->category?></option>
							<? endforeach; ?>
						</select>
						<br><br>
						<? if($room->room_hidden == 1){$HIDDEN = true;}else{$HIDDEN = false;} ?>
						<?=Form::checkbox('room_hidden', 'hidden', $HIDDEN)?>
						<?=Form::label('ルームを公開しない(ルーム一覧に表示されません。)', 'room_hidden')?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
					<?=Form::button('submit', '編集確定', array('class'=>'btn btn-primary'))?>
				</div>
			</div>
		</div>
	<?=Form::close();?>
</div>
<? endif;?>