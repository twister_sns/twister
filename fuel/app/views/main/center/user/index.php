<? if($user_block == false): ?>

	<script>
		var request = [];
		var returnFirst;

		var control = '<?=Uri::segment(1)?>';
		var select = <?if(Uri::segment(2)):?>'<?=Uri::segment(2)?>'<?else:?>''<?endif;?>;
		var mode = <?if(Uri::segment(3)):?>'<?=Uri::segment(3)?>'<?else:?>''<?endif;?>;

		getReadPost(false,1);
		setInterval(function(){getReadPost();},8000);
	</script>


	<div class='post_area'>
		<div class="title">
			<? if(Uri::segment(3) == 'post'): ?>
				すべての投稿
			<? elseif(Uri::segment(3) == 'room_post'): ?>
				ルームの投稿
			<? elseif(Uri::segment(3) == 'was_fav'): ?>
				お気に入りした投稿
			<? elseif(Uri::segment(3) == 'has_fav'): ?>
				お気に入りされた投稿
			<? elseif(Uri::segment(3) == 'was_good'): ?>
				Goodした投稿
			<? elseif(Uri::segment(3) == 'has_good'): ?>
				Goodされた投稿
			<? elseif(Uri::segment(3) == 'mute'): ?>
				ミュートユーザー
			<? else: ?>
				すべての投稿
			<? endif; ?>
		</div>
		<div id='room_post_out'>
			<div class="read_status">
				<div class="loading">
					<div class="progress">
						<div class="progress-bar progress-bar-striped active" aria-valuenow="100">読込中…</div>
					</div>
				</div>
				<div class="no_result">
					<p>表示する結果はありません。</p>
				</div>
			</div>
		</div>
	<div class='nextpage'></div>
	</div>

<? endif; ?>