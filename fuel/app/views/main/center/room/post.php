<script>
	var request = [];
	var returnFirst;
	
	var control = '<?=Uri::segment(1)?>';
	var select = <?if(Uri::segment(2)):?>'<?=Uri::segment(2)?>'<?else:?>''<?endif;?>;
	var mode = <?if(Uri::segment(3)):?>'<?=Uri::segment(3)?>'<?else:?>''<?endif;?>;
	
	getReadPost(false,1);
	getUserStatus(id);
	setInterval(function(){getReadPost();},8000);
</script>

<div style="padding-top:13px;"></div>

<div class='post_area'>
	<div id='room_post_out'>
		<div class="read_status">
			<div class="loading">
				<div class="progress">
					<div class="progress-bar progress-bar-striped active" aria-valuenow="100">読込中…</div>
				</div>
			</div>
			<div class="no_result">
				<p>表示する結果はありません。</p>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(".post_area .title p").fitText(0.6, { minFontSize: '6px', maxFontSize: '20px' });
</script>