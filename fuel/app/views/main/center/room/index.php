<script>
	var request = [];
	var returnFirst;
	
	var select = '<?if(Uri::segment(2) == 'category'):?><?=Uri::segment(3)?><?else:?>0<?endif;?>';
	//var page = <?if(Uri::segment(2)):?>'<?=Uri::segment(2)?>'<?else:?>''<?endif;?>;
	//var page = 1;
	
	getReadRoom();
	getUserStatus(id);
	setInterval(function(){getReadRoom();},8000);
</script>

<div class="MAIN">			
	<div class="roomInformation">
		<div class='pull-left'>
		<div class="roomTitle">
			<h1 class="title"><? if(isset($room_category)): ?><?=$room_category->category?><? else: ?>ルーム<? endif; ?></h1>
		</div>
		<div class="room_description">
			<p class="desc"><? if(isset($room_category)): ?>ルーム数: <?//=count($rooms)?> <? else: ?> ルーム数: <?//=count($rooms)?><? endif; ?></p>
		</div>
		</div>
		<div class='pull-right'>
			<? if(strstr(Uri::current(), 'room')): ?>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#roomCreate">ルーム作成</button>
			<? endif; ?>
		</div>
		<div class='clearBoth'></div>
	</div>
		
	<? /* ?>
	<? foreach($rooms as $room)://print_r($post);exit;?>
		<div class="postWrapper">
			<div class="postFace">
				<img src="http://data.kzho.net/icon/user/<?=$room->user_id?>/50x.png" class="postImg">
			</div>
			<div class="postBody">
				<div class="postContent">
					<div class="postInfo">
						<div class="postUser">
							<a href="http://twister.kzho.net/room/<?=$room->id?>"><span class="room_name"><?=$room->room_name?></span> <span class="user" style='color:#B3B3B3'></span></a>
						</div>
						<div class="postDate">
							<span class="date"><?=Html::anchor('room/category/'.$room->room_category, $room->category->category)?></span>
						</div>
					</div>
					<div class="clearBoth"></div>
					<div class="postArticle">
						<?=nl2br($room->room_desc)?>
					</div>
					<div class="postAction">
						<div class="pull-left">
							<span class='date'>
								<span class='create_user'><? if(count($room->post)){ $end = end($room->post); echo date('m/d H:i', $end->created_at);}else{ echo '無投稿';} ?></span>
								/ <?=count($room->post)?> post
								<div class='hidden-xs inline'>
									/ <?=$room->user->user_name?>
								</div>
							</span>
						</div>
						<div class='pull-right' style='text-align:right;'>
							<span class='glyphicon glyphicon-star action' style='font-size:14px;padding:2px 10px;'></span>		
						</div>
						<div class="clearBoth"></div>
					</div>
				</div>
			</div>
		</div>
	<? endforeach;?>
</div>
<? */ ?>
<div class="post_area">
	<div class="title">
		読み込んでいます…
	</div>
	<div id="room_out">
		<div class="read_status">
			<div class="loading">
				<div class="progress">
					<div class="progress-bar progress-bar-striped active" aria-valuenow="100">読込中…</div>
				</div>
			</div>
			<div class="no_result">
				<p>表示する結果はありません。</p>
			</div>
		</div>
	</div>
</div>
<div class="nextpage"></div>
	
<!-- ルーム作成・編集モダル -->
<div class="modal fade" id="roomCreate" tabindex="-1" role="dialog" aria-labelledby="roomCreateLabel" aria-hidden="true">
	<?=Form::open(array('action'=>'api/room_control.json', 'id'=>'form_room_create'))?>
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">ルームを作成する！</h4>
				</div>
				<div class="modal-body">
					<?=Form::hidden('mode', 'create')?>
					<div class='row'>
						<div class='col-lg-12' style='text-align:left;'>
						<?=Form::label('ルーム名前', 'room_name')?>
						<?=Form::input('room_name', Input::post('user_name'), array('class'=>'form-control', 'id'=>'TCLIUserName', 'placeholder'=>'～64文字'))?>
						<br>
						<?=Form::label('ルーム説明', 'room_desc')?>
						<?=Form::textarea('room_desc', '', array('class'=>'form-control', 'rows'=>'3', 'placeholder'=>'～1024文字', 'id'=>'form_room_desc'))?>
						<br>
						<?=Form::label('ルームカテゴリ', 'room_category')?><br>
						<select name="form_room_category" id="form_room_category" class='form-control'>
							<? foreach($room_categories as $key => $category): ?>
								<? if($category->id == 1){continue;} ?>
								<option value="<?=$category->id?>" style="text-indent: 0px;"><?=$category->category?></option>
							<? endforeach; ?>
						</select>
						<br><br>
						<?=Form::checkbox('room_hidden', 'hidden')?>
						<?=Form::label('ルームを公開しない(ルーム一覧に表示されません。)', 'room_hidden')?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
					<?=Form::button('submit', '作成する', array('class'=>'btn btn-primary'))?>
				</div>
			</div>
		</div>
	<?=Form::close();?>
</div>
</div>