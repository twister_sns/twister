<div class="MAIN">			
	<div class="roomInformation">
		<div class="roomTitle">
			<h1 class="title">プロフィール</h1>
		</div>
		<div class="roomDescription">
			<p class="desc">注意:チェック不足・未完成の為エラーが発生することがあります。</p>
		</div>
	</div>
	<div class="blankWrapper">
		<?if(isset($outMessage)):?>
			<div class="alert alert-info" role="alert">
				<?=$outMessage?>
				<button type="button" data-dismiss="alert" class="close">
					<span>×</span>
				</button>
			</div>
		<?endif;?>
		<p class="bp">設定を編集後、保存ボタンを押して下さい。</p>
		<?=Form::open(array('action'=>'setting/profile', 'enctype'=>'multipart/form-data'))?>
		<table class='setting'>
			<tr>
				<td class='left'>
					<p class="cell_head">アイコン</p>
				</td>
				<td class='right'>
					<?=Form::file('user_icon', array('class'=>'form-control', 'style'=>'width:100%;'))?>
				</td>
			</tr>
			<tr>
				<td class='left'>
					<p class="cell_head">背景画像</p>
				</td>
				<td class='right'>
					<?=Form::file('user_background', array('class'=>'form-control', 'style'=>'width:100%;'))?>
				</td>
			</tr>
			<tr>
				<td class='left'>
					<p class="cell_head">名前</p>
				</td>
				<td class='right'>
					<?=Form::input('user_name', Auth::get_user_name(), array('class'=>'form-control', 'maxlength'=>'64'))?>
					<span class='notice'>※64文字まで使用出来ます。</span>
				</td>
			</tr>
			<tr>
				<td class='left'>
					<p class="cell_head">メッセージ</p>
				</td>
				<td class='right'>
					<dd>
						<?=Form::textarea('user_desc', Auth::get_user_desc(), array('class'=>'form-control', 'rows'=>'3'))?>
					</dd>
				</td>
			</tr>
		</table>
		<?=Form::button('submit', '保存する', array('class'=>'btn btn-success btn-block'))?>
		<?=Form::close()?>
	</div>
</div>