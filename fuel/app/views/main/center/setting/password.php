<div class="MAIN">			
	<div class="roomInformation">
		<div class="roomTitle">
			<h1 class="title">パスワードの変更</h1>
		</div>
		<div class="roomDescription">
			<p class="desc">注意:チェック不足・未完成の為エラーが発生することがあります。</p>
		</div>
	</div>
	<div class="blankWrapper">
		<?if(isset($outMessage)):?>
			<div class="alert alert-info" role="alert">
				<?=$outMessage?>
				<button type="button" data-dismiss="alert" class="close">
					<span>×</span>
				</button>
			</div>
		<?endif;?>
		<p class="bp">設定を編集後、保存ボタンを押して下さい。</p>
		<?=Form::open(array('action'=>'setting/password', 'enctype'=>'multipart/form-data'))?>
		<table class='setting'>
			<tr>
				<td class='left'>
					<p class="cell_head">現在のパスワード</p>
				</td>
				<td class='right'>
					<dd>
					<?=Form::password('user_old_pass', '', array('class'=>'form-control', 'maxlength'=>'32'))?>
					</dd>
				</td>
			</tr>
			<tr>
				<td class='left'>
					<p class="cell_head">新しいパスワード</p>
				</td>
				<td class='right'>
					<dd>
					<?=Form::password('user_new_pass', '', array('class'=>'form-control', 'maxlength'=>'32'))?>
					<span class='notice'>※32文字まで使用出来ます。</span>
					</dd>
				</td>
			</tr>
			<tr>
				<td class='left'>
					<p class="cell_head">再入力</p>
				</td>
				<td class='right'>
					<?=Form::password('user_new_pass_re', '', array('class'=>'form-control', 'maxlength'=>'32'))?>
				</td>
			</tr>
		</table>
		<?=Form::button('submit', '保存する', array('class'=>'btn btn-success btn-block'))?>
		<?=Form::close()?>
	</div>
</div>