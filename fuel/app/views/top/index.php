<div class="topContainer">
	<? /* 会員登録の場合、アニメーションを停止 */ ?>
	<? if(Input::post('TCLType') == 'regist'): ?>
		<script>
			$(window).load(function(){
				$("input[name='TCLType']").val(["regist"]);
				$("#TCLMode").html("新規登録！");
				$(".TCLRegistExt").show();
			});
		</script>
	<? endif; ?>
	<? //*/ ?>
	
	<div class="jumbotron TC hidden-sm hidden-xs">
		<div class="container">
			<h1 style="font-size:40px;">ああああああああああああああああああああああああああ</h1>
			<h2>Twister</h2>
		</div>
	</div>
	
	
	<div class="topContainerBox">
		<div class="title" id="TCLMode">ログイン</div>
		<div class="wrapper">
			<div class="body">
				<? //FORMクラス ?>
				<?=Form::open('top/send')?>
					<div class="switch-toggle well">
						<?=Form::radio('TCLType', 'login', array('class'=>'', 'id'=>'login'))?>
						<?=Form::label('ログイン', '', array('for'=>'login', 'style'=>'width:9999px;font-size:19px;'))?>
						<?=Form::radio('TCLType', 'regist', true, array('class'=>'', 'id'=>'regist'))?>
						<?=Form::label('新規登録！', '', array('for'=>'regist', 'style'=>'width:9999px;font-size:19px;'))?>
						<a class="btn btn-primary"></a>
					</div>
					<? if(isset($authMessage)): ?>
						<div class="alert alert-error TCLAlert">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<span style="font-size:19px;font-weight:bold;">ご確認下さい</span><br>
							<?=$authMessage?>
						</div>
					<? endif; ?>
					<div class="form-group TCLRegistExt" style="display: none;">
						<?=Form::label('ユーザ名', 'user_name')?>
						<?=Form::input('user_name', Input::post('user_name'), array('class'=>'form-control', 'id'=>'TCLIUserName', 'placeholder'=>'1-64字まで入力できます'))?>
					</div>
					<div class="form-group">
						<?=Form::label('ユーザID', 'user_id')?>
						<?=Form::input('user_id', Input::post('user_id'), array('class'=>'form-control', 'id'=>'TCLIUserId', 'placeholder'=>'半角英数字を2-32字まで入力できます'))?>
					</div>
					<div class="form-group">
						<?=Form::label('パスワード', 'user_pass')?>
						<?=Form::password('user_pass', '', array('class'=>'form-control', 'id'=>'TCLIUserPass', 'placeholder'=>'半角英数字を4-128字まで入力できます'))?>
					</div>
					<div class="form-group TCLRegistExt" style="display: none;">
						<?=Form::label('パスワード(再入力)', 'user_pass_retype')?>
						<?=Form::password('user_pass_retype', '', array('class'=>'form-control', 'id'=>'TCLIUserPassRetype', 'placeholder'=>'再度入力して下さい'))?>
					</div>
					<div style="text-align: center;">
					</div>
					<?=Form::button('submit', '送信する', array('class'=>'btn btn-success btn-block'))?>
				<?=Form::close()?>
			</div>
		</div>
	</div>
</div>







<?/*

				<!--
				<div class="switch-toggle well">
					<input id="login" name="TCLType" type="radio" value="login">
					<label for="login" onclick="" style="width:9999px;font-size:19px;">ログイン</label>

					<input id="regist" name="TCLType" type="radio" value="regist" checked>
					<label for="regist" onclick="" style="width:9999px;font-size:19px;">新規登録！</label>

					<a class="btn btn-primary"></a>
				</div>
				<form action="send" method="post" role="form">
					<div class="form-group TCLRegistExt" style="display: none;">
					<label for="user_name">ユーザー名</label>
					<input type="password" name="user_name" class="form-control" id="exampleInputPassword1" placeholder="16字まで入力できます">
					</div>
					<div class="form-group">
					<label for="user_id">ユーザーID</label>
					<input type="email" name="user_id" class="form-control" id="exampleInputEmail1" placeholder="半角英数字が16字まで使用出来ます">
					</div>
					<div class="form-group">
					<label for="user_pass">パスワード</label>
					<input type="password" name="user_pass" class="form-control" id="exampleInputPassword1" placeholder="半角英数字が16字まで使用出来ます">
					</div>
					<div class="form-group TCLRegistExt" style="display: none;">
					<label for="user_pass_retype">パスワード (再度入力)</label>
					<input type="password" name="user_pass_retype" class="form-control" id="exampleInputPassword1" placeholder="再度入力して下さい">
					</div>
					<div class="checkbox" style="text-align:center;">
					<label>
						<input type="checkbox">ログイン状態を保持する
					</label>
					</div>
					<button type="submit" class="btn btn-success btn-block">送信</button>
				</form>
				-->
*/?>