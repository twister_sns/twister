<? if($navbar_mode == 'user'): ?>

<nav class="navigation_bar_wrapper sb-slide">
	<div class="navigation_bar top">
		<div class="left_area">
			<?//=Html::anchor('/', 'Twister', array('class'=>'site_top'))?>
			<?=Html::anchor('/', '<img src="http://dev_twister.kzho.net/assets/img/logo.png" style="height:25px;width:auto;">', array('class'=>'site_top'))?>
			<ul class="left desktop_menu">
				<li><?=Html::anchor('/', '<span class="glyphicon glyphicon-home"></span>ホーム')?></li
				><!--<li><?=Html::anchor('/', '通知')?></li
				>--><li><?=Html::anchor('room', '<span class="glyphicon glyphicon-list-alt"></span>ルーム')?></li
				><li><?=Html::anchor('public', '<span class="glyphicon glyphicon-globe"></span>パブリック')?></li
				><!--<li><?=Html::anchor('/', 'メッセージ')?></li>-->
			</ul>
		</div>
		<div class="right_area">
			<div class="right desktop_menu">
				<ul id="dropdown">
					<li><a style="text-align:right;cursor: pointer;">アカウント▼</a>
						<ul>
							<li><?=Html::anchor('/', 'マイページ')?></li>
							<li><?=Html::anchor('/', 'プロフィール')?></li>
							<li><?=Html::anchor('setting/profile', '設定')?></li>
							<li><?=Html::anchor('/', 'ファイル管理')?></li>
							<li><?=Html::anchor('logout', 'ログアウト')?></li>
						</ul>
					</li>
				</ul>
			</div>
			<ul class="right mobile_menu">
				<li><a class="sb-toggle-left" href="javascript::void();"><span class="glyphicon glyphicon-align-justify navigation_bar_icon" aria-hidden="true"></span>メニュー</a></li
				><li><a href="http://twister.kzho.net/"><span class="glyphicon glyphicon-home navigation_bar_icon" aria-hidden="true"></span>ホーム</a></li
				><li><a href="http://twister.kzho.net/room"><span class="glyphicon glyphicon-list-alt navigation_bar_icon" aria-hidden="true"></span>ルーム</a></li
				><!--<li><a href=""><span class="glyphicon glyphicon-edit navigation_bar_icon" aria-hidden="true"></span>投稿</a></li> -->
			</ul>
		</div>
	</div>
	<div style="clear:both;"></div>
</nav>

<div class="sb-slidebar sb-left">
	<h1>ユーザー</h1>
		<div class="slidebar_profile_box">
			<div class="name">
				<? if(isset($postUser)): ?>
					<p class="user_name"><?=Html::anchor('user/'.$postUser['user_id'], $postUser['user_name'])?></p>
					<p class="user_id"><?=Html::anchor('user/'.$postUser['user_id'], '@'.$postUser['user_id'])?></p>
				<? else: ?>
					<p class="user_name"><?=Html::anchor('user/'.$user['user_id'], $user['user_name'])?></p>
					<p class="user_id"><?=Html::anchor('user/'.$user['user_id'], '@'.$user['user_id'])?></p>
				<? endif;?>
			</div>
			<div class="face">
				<? if(isset($postUser)): ?>
					<?=Html::anchor('user/'.$postUser['user_id'], '<img src="http://data.kzho.net/icon/user/'.$postUser['id'].'/64x.png" style="width:62px;height:62px;">')?>
				<? else: ?>
					<?=Html::anchor('user/'.$user['user_id'], '<img src="http://data.kzho.net/icon/user/'.$user['id'].'/64x.png" style="width:62px;height:62px;">')?>
				<? endif;?>
			</div>
			<div class="status">
				<p class="profile_box_user_post int profile_box_status" id="">取得中…</p>
				<p class="desc">投稿</p>
			</div>
			<div class="status">
				<p class="profile_box_user_level int profile_box_status" id="">取得中…</p>
				<p class="desc">Lv</p>
			</div>
			<div class="status">
				<p class="profile_box_user_exp int profile_box_status" id="">取得中…</p>
				<p class="desc">Exp</p>
			</div>
			<div style="clear:Both;"></div>
			<? if($user['id'] == 0): ?>
				<ul>
					<li><a href="http://twister.kzho.net/" class=""><span class="glyphicon glyphicon-globe slide_bar_icon" aria-hidden="true"></span>ログイン/新規登録</a></li>
				</ul>
			<? endif;?>
		</div>
	<h1>メニュー</h1>
	<ul>
		<!--<li><a href="" class="u_c"><span class="glyphicon glyphicon-envelope slide_bar_icon" aria-hidden="true"></span>通知</a></li-->
		<li><?=Html::anchor('/', '<span class="glyphicon glyphicon-envelope slide_bar_icon" aria-hidden="true"></span>通知', array('class'=>'u_c'))?></li
		><li><?=Html::anchor('room', '<span class="glyphicon glyphicon-list-alt slide_bar_icon" aria-hidden="true"></span>ルーム', array('class'=>''))?></li
		><li><?=Html::anchor('/','<span class="glyphicon glyphicon-heart slide_bar_icon" aria-hidden="true"></span>お気に入り',array('class'=>'u_c'))?></li
		><li><?=Html::anchor('/','<span class="glyphicon glyphicon-th-list slide_bar_icon" aria-hidden="true"></span>リスト',array('class'=>'u_c'))?></li
		><li><?=Html::anchor('/','<span class="glyphicon glyphicon-search slide_bar_icon" aria-hidden="true"></span>検索',array('class'=>'u_c'))?></li
		><li><?=Html::anchor('/','<span class="glyphicon glyphicon-user slide_bar_icon" aria-hidden="true"></span>マイページ',array('class'=>'u_c'))?></li
		><li><?=Html::anchor('setting/profile','<span class="glyphicon glyphicon-cog slide_bar_icon" aria-hidden="true"></span>設定')?></li>
	</ul>

	<script type="text/javascript">
		$(".slidebar_profile_box .status p.int").fitText(1.0, { minFontSize: '14px', maxFontSize: '24px' });
	</script>
</div>
<? else: ?>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<!-- スマートフォンサイズで表示されるメニューボタンとテキスト -->
			<div class="navbar-header">
				<!--
					メニューボタン
					data-toggle : ボタンを押したときにNavbarを開かせるために必要
					data-target : 複数navbarを作成する場合、ボタンとナビを紐づけるために必要
				-->
				<!--
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-menu-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				-->
				<!-- タイトルなどのテキスト -->
				<button type="button" class="btn btn-primary navbar-btn" onclick="location.href='http://twister.kzho.net/'">新規登録 / ログイン</button>
				<?=Html::anchor('/', '<img src="http://dev_twister.kzho.net/assets/img/logo.png" style="height:25px;width:auto;">', array('class'=>'navbar-brand'))?>
			</div>
		</div>
	</nav>
<? endif; ?>