<?php
/**
 * Fuel
 *
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.7
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2015 Fuel Development Team
 * @link       http://fuelphp.com
 */

namespace Auth;

/**
 * SimpleAuth basic login driver
 *
 * @package     Fuel
 * @subpackage  Auth
 */
class Auth_Login_Simpleauth extends \Auth_Login_Driver
{
	/**
	 * Load the config and setup the remember-me session if needed
	 */
	public static function _init()
	{
		\Config::load('simpleauth', true);

		// setup the remember-me session object if needed
		if (\Config::get('simpleauth.remember_me.enabled', false))
		{
			static::$remember_me = \Session::forge(array(
				'driver' => 'cookie',
				'cookie' => array(
					'cookie_name' => \Config::get('simpleauth.remember_me.cookie_name', 'rmcookie'),
				),
				'encrypt_cookie' => true,
				'expire_on_close' => false,
				'expiration_time' => \Config::get('simpleauth.remember_me.expiration', 86400 * 729),
			));
		}
	}

	/**
	 * @var  Database_Result  when login succeeded
	 */
	protected $user = null;

	/**
	 * @var  array  value for guest login
	 */
	protected static $guest_login = array(
		'id' => 0,
		'user_id' => 'guest',
		'group' => '0',
		'login_hash' => false,
		'email' => false,
	);

	/**
	 * @var  array  SimpleAuth class config
	 */
	protected $config = array(
		'drivers' => array('group' => array('Simplegroup')),
		'additional_fields' => array('profile_fields'),
	);

	/**
	 * ログイン 確認
	 *
	 * @return  bool
	 */
	protected function perform_check()
	{
		// セッションから user_id, login_hash を取得
		$user_id    = \Session::get('user_id');
		$login_hash  = \Session::get('login_hash');
		

		// only worth checking if there's both a user_id and login-hash
		if ( ! empty($user_id) and ! empty($login_hash))
		{
			if (is_null($this->user) or ($this->user['user_id'] != $user_id and $this->user != static::$guest_login))
			{
				$this->user = \DB::select_array(\Config::get('simpleauth.table_columns', array('*')))
					->where('user_id', '=', $user_id)
					->from(\Config::get('simpleauth.table_name'))
					->execute(\Config::get('simpleauth.db_connection'))->current();
			}

			// return true when login was verified, and either the hash matches or multiple logins are allowed
			if ($this->user and (\Config::get('simpleauth.multiple_logins', false) or $this->user['login_hash'] === $login_hash or $this->user['user_id'] === $user_id))
			{
				return true;
			}
		}

		// not logged in, do we have remember-me active and a stored user_id?
		elseif (static::$remember_me and $user_id = static::$remember_me->get('user_id', null))
		{
			return $this->force_login($user_id);
		}

		// no valid login when still here, ensure empty session and optionally set guest_login
		$this->user = \Config::get('simpleauth.guest_login', true) ? static::$guest_login : false;
		\Session::delete('user_id');
		\Session::delete('login_hash');

		return false;
	}

	/**
	 * Check the user exists
	 *
	 * @return  bool
	 */
	public function validate_user($user_id = '', $user_pass = '')
	{
		$user_id = trim($user_id) ?: trim(\Input::post(\Config::get('simpleauth.username_post_key', 'user_id')));
		$user_pass = trim($user_pass) ?: trim(\Input::post(\Config::get('simpleauth.password_post_key', 'user_pass')));

		if (empty($user_id) or empty($user_pass))
		{
			return false;
		}

		$user_pass = $this->hash_password($user_pass);
		$user = \DB::select_array(\Config::get('simpleauth.table_columns', array('*')))
			->where_open()
			->where('user_id', '=', $user_id)
			->where_close()
			->where('user_pass', '=', $user_pass)
			->from(\Config::get('simpleauth.table_name'))
			->execute(\Config::get('simpleauth.db_connection'))->current();

		return $user ?: false;
	}

	/**
	 * ログイン
	 *
	 * @param   string
	 * @param   string
	 * @return  bool
	 */
	public function login($user_id = '', $user_pass = '')
	{
		if ( ! ($this->user = $this->validate_user($user_id, $user_pass)))
		{
			$this->user = \Config::get('simpleauth.guest_login', true) ? static::$guest_login : false;
			\Session::delete('user_id');
			\Session::delete('login_hash');
			return false;
		}

		// register so Auth::logout() can find us
		Auth::_register_verified($this);

		\Session::set('user_id', $this->user['user_id']);
		\Session::set('login_hash', $this->create_login_hash());
		\Session::instance()->rotate();
		return true;
	}

	/**
	 * 強制ログイン
	 *
	 * @param   string
	 * @return  bool
	 */
	public function force_login($user_id = '')
	{
		if (empty($user_id))
		{
			return false;
		}

		$this->user = \DB::select_array(\Config::get('simpleauth.table_columns', array('*')))
			->where_open()
			->where('id', '=', $user_id)
			->where_close()
			->from(\Config::get('simpleauth.table_name'))
			->execute(\Config::get('simpleauth.db_connection'))
			->current();

		if ($this->user == false)
		{
			$this->user = \Config::get('simpleauth.guest_login', true) ? static::$guest_login : false;
			\Session::delete('user_id');
			\Session::delete('login_hash');
			return false;
		}

		\Session::set('user_id', $this->user['user_id']);
		\Session::set('login_hash', $this->create_login_hash());

		// and rotate the session id, we've elevated rights
		\Session::instance()->rotate();

		// register so Auth::logout() can find us
		Auth::_register_verified($this);

		return true;
	}

	/**
	 * ログアウト
	 *
	 * @return  bool
	 */
	public function logout()
	{
		//$this->user = \Config::get('simpleauth.guest_login', true) ? static::$guest_login : false;
		\Session::delete('user_id');
		\Session::delete('login_hash');
		//\Cookie::delete('fuelcid');
		return true;
	}

	/**
	 * ユーザ 作成
	 *
	 * @param   string
	 * @param   string
	 * @param   string
	 * @param   int     group id
	 * @return  bool
	 */
	public function create_user($user_id, $user_pass, $user_name, $group = 1)
	{
		$user_pass = trim($user_pass);
		if (empty($user_id) or empty($user_pass))
		{
			throw new \SimpleUserUpdateException('ユーザID、又はパスワードが入力されていません。入力内容をご確認下さい。', 1);
		}

		$same_users = \DB::select_array(\Config::get('simpleauth.table_columns', array('*')))
			->where('user_id', '=', $user_id)
			->from(\Config::get('simpleauth.table_name'))
			->execute(\Config::get('simpleauth.db_connection'));

		if ($same_users->count() > 0)
		{
				throw new \SimpleUserUpdateException('入力したユーザIDは既に存在します。違うユーザIDで登録することが出来ます。', 3);
		}
		
		$user = array(
			'user_id'        => (string) $user_id,
			'user_name'		=> htmlspecialchars($user_name,ENT_QUOTES),
			'user_pass'        => $this->hash_password((string) $user_pass),
			'user_desc'           => '',
			'user_exp'           => (int) 0,
			'group'           => (int) $group,
			'last_login'      => 0,
			'login_hash'      => sha1(\Config::get('simpleauth.login_hash_salt').$user_id.$user_pass),
			'created_at'      => \Date::forge()->get_timestamp(),
		);
		$result = \DB::insert(\Config::get('simpleauth.table_name'))
			->set($user)
			->execute(\Config::get('simpleauth.db_connection'));


		/* ★追加：ログインハッシュの生成
		$login_hash = sha1(\Config::get('simpleauth.login_hash_salt').$user_id.$user_pass);
		\DB::update(\Config::get('simpleauth.table_name'))
			->set(array('login_hash' => $login_hash))
			->where('user_id', '=', $user_id)
			->execute(\Config::get('simpleauth.db_connection'));
		 */

		return ($result[1] > 0) ? $result[0] : false;
	}

	/**
	 * Update a user's properties
	 * Note: user_id cannot be updated, to update password the old password must be passed as old_password
	 *
	 * @param   Array  properties to be updated including profile fields
	 * @param   string
	 * @return  bool
	 */
	public function update_user($values, $user_id = null)
	{
		$user_id = $user_id ?: $this->user['user_id'];
		$current_values = \DB::select_array(\Config::get('simpleauth.table_columns', array('*')))
			->where('user_id', '=', $user_id)
			->from(\Config::get('simpleauth.table_name'))
			->execute(\Config::get('simpleauth.db_connection'));

		if (empty($current_values))
		{
			throw new \SimpleUserUpdateException('ユーザIDが存在しません。', 4);
		}

		$update = array();
		if (array_key_exists('user_id', $values))
		{
			throw new \SimpleUserUpdateException('ユーザIDは変更することが出来ません。', 5);
		}
		if (array_key_exists('user_pass', $values))
		{
			if (empty($values['old_password'])
				or $current_values->get('user_pass') != $this->hash_password(trim($values['old_password'])))
			{
				throw new \SimpleUserWrongPassword('古いパスワードは無効です。');
			}

			$user_pass = trim(strval($values['user_pass']));
			if ($user_pass === '')
			{
				throw new \SimpleUserUpdateException('パスワードは空にすることはできません。', 6);
			}
			$update['user_pass'] = $this->hash_password($user_pass);
			
			// ★追加：パスワード変更時にハッシュの再生成を行う
			$update['login_hash'] = sha1(\Config::get('simpleauth.login_hash_salt').$user_id.$user_pass);
			unset($values['user_pass']);
		}
		if (array_key_exists('old_password', $values))
		{
			unset($values['old_password']);
		}
		if (array_key_exists('group', $values))
		{
			if (is_numeric($values['group']))
			{
				$update['group'] = (int) $values['group'];
			}
			unset($values['group']);
		}
		/*
		if ( ! empty($values))
		{
			$profile_fields = @unserialize($current_values->get('profile_fields')) ?: array();
			foreach ($values as $key => $val)
			{
				if ($val === null)
				{
					unset($profile_fields[$key]);
				}
				else
				{
					$profile_fields[$key] = $val;
				}
			}
			$update['profile_fields'] = serialize($profile_fields);
		}
		*/
		$update['updated_at'] = \Date::forge()->get_timestamp();

		$affected_rows = \DB::update(\Config::get('simpleauth.table_name'))
			->set($update)
			->where('user_id', '=', $user_id)
			->execute(\Config::get('simpleauth.db_connection'));

		// Refresh user
		if ($this->user['user_id'] == $user_id)
		{
			$this->user = \DB::select_array(\Config::get('simpleauth.table_columns', array('*')))
				->where('user_id', '=', $user_id)
				->from(\Config::get('simpleauth.table_name'))
				->execute(\Config::get('simpleauth.db_connection'))->current();
		}

		return $affected_rows > 0;
	}

	/**
	 * パスワードの変更
	 *
	 * @param   string
	 * @param   string
	 * @param   string  user_id or null for current user
	 * @return  bool
	 */
	public function change_password($old_password, $new_password, $user_id = null)
	{
		try
		{
			return (bool) $this->update_user(array('old_password' => $old_password, 'user_pass' => $new_password), $user_id);
		}
		//例外
		catch (SimpleUserWrongPassword $e)
		{
			return false;
		}
	}

	/**
	 * Generates new random password, sets it for the given user_id and returns the new password.
	 * To be used for resetting a user's forgotten password, should be emailed afterwards.
	 *
	 * @param   string  $user_id
	 * @return  string
	 */
	public function reset_password($user_id)
	{
		$new_password = \Str::random('alnum', 8);
		$password_hash = $this->hash_password($new_password);

		$affected_rows = \DB::update(\Config::get('simpleauth.table_name'))
			->set(array('user_pass' => $password_hash))
			->where('user_id', '=', $user_id)
			->execute(\Config::get('simpleauth.db_connection'));

		if ( ! $affected_rows)
		{
			throw new \SimpleUserUpdateException('Failed to reset password, user was invalid.', 8);
		}

		return $new_password;
	}

	/**
	 * Deletes a given user
	 *
	 * @param   string
	 * @return  bool
	 */
	public function delete_user($user_id)
	{
		if (empty($user_id))
		{
			throw new \SimpleUserUpdateException('Cannot delete user with empty user_id', 9);
		}

		$affected_rows = \DB::delete(\Config::get('simpleauth.table_name'))
			->where('user_id', '=', $user_id)
			->execute(\Config::get('simpleauth.db_connection'));

		return $affected_rows > 0;
	}

	/**
	 * ログインの検証、ハッシュの作成
	 *
	 * @return  string
	 */
	public function create_login_hash()
	{
		if (empty($this->user))
		{
			throw new \SimpleUserUpdateException('ログインして下さい。', 10);
		}

		$last_login = \Date::forge()->get_timestamp();
		$login_hash = sha1(\Config::get('simpleauth.login_hash_salt').$this->user['user_id'].$this->user['user_pass']);
/*
		\DB::update(\Config::get('simpleauth.table_name'))
			->set(array('last_login' => $last_login, 'login_hash' => $login_hash))
			->where('user_id', '=', $this->user['user_id'])
			->execute(\Config::get('simpleauth.db_connection'));
*/
		$this->user['login_hash'] = $login_hash;

		return $login_hash;
	}

	/**
	 * Get the user's ID
	 *
	 * @return  Array  containing this driver's ID & the user's ID
	public function get_user_id()
	{
		if (empty($this->user))
		{
			return false;
		}

		return array($this->id, (int) $this->user['id']);
	}
	 */

	/**
	 * Get the user's groups
	 *
	 * @return  Array  containing the group driver ID & the user's group ID
	 */
	public function get_groups()
	{
		if (empty($this->user))
		{
			return false;
		}

		return array(array('Simplegroup', $this->user['group']));
	}

	/**
	 * Getter for user data
	 *
	 * @param  string  name of the user field to return
	 * @param  mixed  value to return if the field requested does not exist
	 *
	 * @return  mixed
	 */
	public function get($field, $default = null)
	{
		if (isset($this->user[$field]))
		{
			return $this->user[$field];
		}
		elseif (isset($this->user['profile_fields']))
		{
			return $this->get_profile_fields($field, $default);
		}

		return $default;
	}

	/**
	 * 削除禁止
	 *
	 * Get the user's emailaddress
	 *
	 * @return  string
	 */
	public function get_email()
	{
		return $this->get('email', false);
	}

	/**
	 * ユーザ 登録番号を取得
	 *
	 * @return  string
	 */
	public function get_uid()
	{
		return $this->get('id', false);
	}

	/**
	 * ユーザ IDを取得
	 *
	 * @return  string
	 */
	public function get_user_id()
	{
		return $this->get('user_id', false);
	}

	/**
	 * ユーザ 名を取得
	 *
	 * @return  string
	 */
	public function get_user_name()
	{
		return $this->get('user_name', false);
	}

	/**
	 * ユーザ 説明を取得
	 *
	 * @return  string
	 */
	public function get_user_desc()
	{
		return $this->get('user_desc', false);
	}

	/**
	 * 削除禁止
	 *
	 * Get the user's screen name
	 *
	 * @return  string
	 */
	public function get_screen_name()
	{
		if (empty($this->user))
		{
			return false;
		}

		return $this->user['user_id'];
	}

	/**
	 * 使用しない
	 *
	 * Get the user's profile fields
	 *
	 * @return  Array
	 */
	public function get_profile_fields($field = null, $default = null)
	{
		if (empty($this->user))
		{
			return false;
		}

		if (isset($this->user['profile_fields']))
		{
			is_array($this->user['profile_fields']) or $this->user['profile_fields'] = (@unserialize($this->user['profile_fields']) ?: array());
		}
		else
		{
			$this->user['profile_fields'] = array();
		}

		return is_null($field) ? $this->user['profile_fields'] : \Arr::get($this->user['profile_fields'], $field, $default);
	}

	/**
	 * Extension of base driver method to default to user group instead of user id
	 */
	public function has_access($condition, $driver = null, $user = null)
	{
		if (is_null($user))
		{
			$groups = $this->get_groups();
			$user = reset($groups);
		}
		return parent::has_access($condition, $driver, $user);
	}

	/**
	 * Extension of base driver because this supports a guest login when switched on
	 */
	public function guest_login()
	{
		return \Config::get('simpleauth.guest_login', true);
	}
}

// end of file simpleauth.php
