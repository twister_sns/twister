var page = 1;
var teiji = $("title").text();
var alertType = '';
var returnTitle = 0;
var returnCount = 0;
var returnArray = new Array();
var returnFirst;
var desktopCheck = false;
var add_files_field = $(".add_files").val();
var readSwitch = true;
var TwiURL = 'http://' + document.domain + '/';


$(function(){

  /*
   * 初期 / 定期実行
   */
  $.slidebars();

  /*
   *  共通 / ウィンドウ表示チェック
   */
  setInterval(function(){displayCheck();},5000);

  /*
   * 共通 / ファイルアップロード
   */
  $(".dropzone").dropzone({
    url: TwiURL + "api/file_upload",
    parallelUploads: 2,
    maxThumbnailFilesize: 10,
    dictDefaultMessage:'選択･ドロップ',
    dictFileTooBig:'100MBまでのファイルをアップロードできます',

    success: function(file, response){
      if (file.previewElement) {
        file.previewElement.classList.add("dz-success");
      }
      if(response['upload_id'] != null){
        $(".add_files").val($(".add_files").val() + '[F]' + response['upload_id'] + '[/F]');
      }
    }
  });

  /*
   * ドロップダウン メニュー
   */
  $(".navigation_bar .desktop_menu ul#dropdown li").hover(function() {
    $(this).children('ul').show();
  }, function() {
    $(this).children('ul').hide();
  });


  /*
   * 共通 / サイドバー
   */

  $(window).on('load resize', function(){
    if($(window).width() > 1026){
      //desktopCheck = true;
      //heightCatch(desktopCheck);
    }else{
      //desktopCheck = false;
      //heightCatch(desktopCheck);
    }
  });

  /*
   *  共通 / 投稿イベント
   */
  $('#form_room_post').submit(function(event){
    event.preventDefault();
    var formObject = $('#form_room_post');
    if($('#form_message').val().length == 0 && $('.add_files').val().length == 0){
      alert('文字を入力して下さい。');
      return false;
    }
    $('#room_post_submit').prop('disabled', true);
    $('#room_post_submit').text('投稿しています…');
    $.ajax({
      url: TwiURL + "api/room_post.json",
      type: "POST",
      data: formObject.serializeArray()
    }).always(function(){
      setTimeout(function(){$('#room_post_submit').prop('disabled', false);}, 2000);
      $('#room_post_submit').text('投稿');
    }).done(function(data){
      twisterAlert('投稿が完了しました！', 'success', 1500);
      $("#form_message").val('');
      $(".add_files").val('');
      $(".dropzone-previews").html('');
      $('#form_message_decolation').hide();
      getReadPost('admin_fanclub');
      getUserStatus(id);
    }).fail(function(){
      twisterAlert('投稿に失敗しました。認証が解除されていませんか？', 'danger', 5000);
    });
  });

  /*
   * 共通 / 次ページ読込
   */
  $('#room_post_out').on('inview', '.nextpage', function() {
    console.info('next page');
    page++;
    if((typeof(room_category) != "undefined")){
      getReadRoom();
    }else{
      getReadPost(false, page);
    }
  });

  /*
   * 共通 / 詳細開閉
   */
  $('#room_desc_open').on('click', function(){
    if($('.room_description').css('display') == 'block'){
      $('.room_description').hide();
      $('#room_desc_open').html('<span class="glyphicon glyphicon-chevron-down">');
    }else{
      $('.room_description').show();
      $('#room_desc_open').html('<span class="glyphicon glyphicon-chevron-up">');
    }
    return false;
  });

  /*
   * 投稿 / ショートカットキー
   */
  $(window).keydown(function(e){
    if(document.activeElement.id == 'form_message'){
      if(e.ctrlKey){
        if(e.keyCode === 13){
          $('#form_room_post').trigger("submit");
          return false;
        }
      }
      if(e.shiftKey){
        if(e.keyCode === 13){
          $('#form_room_post').trigger("submit");
          return false;
        }
      }
    }
  });

  /*
   * ユーザー / モダルアクション
   */
  $('#user_action').click(function(e){
    e.preventDefault();
    $('.modal_action_text').html('');
    $('.modal_action_alert').hide();
    var action = $(this).attr('name');
    action = action.split('-');
    $.ajax({
      url: TwiURL + "api/user_action.json",
      type: "POST",
      data: {action:action[0],value:action[1]}
    }).always(function(){
      $('.modal_action_alert').show();
    }).done(function(data){
      if(data['result'] == true){
        $('.modal_action_text').html('処理が完了しました。リロードします…');
        setInterval(function(){location.reload();},500);
      }else{
        $('.modal_action_text').html('処理が完了できませんでした。');
      }
    }).fail(function(){
      $('.modal_action_text').html('処理が完了出来ませんでした。kzho.netへ接続出来ません。');
    });
  });

  /*
   * 投稿 / アクション
   */
  $("#room_post_out").on('click','span.js-post-action', function(){
    var mes = new Array();
    var split = $(this).attr('id').split('-');
    var action = split[0];
    var value = split[1];
    console.debug('投稿アクションを実行 (' + '投稿:' + value + ' アクション:' + action + ')');
    $.ajax({
      url: TwiURL + "api/user_action.json",
      type: "GET",
      data: {action:action,value:value}
    }).done(function(data){
      if(data['return'] == true){
        console.info('投稿アクション成功');
        switch (action){
          case ('good'):
            switch (data['result']){
              case 1:
                $('#good-'+value).text(Number($('#good-'+value).text())+1);
                $('#good-'+value).css('color','orange');
                break;
              case 2:
                if(Number($('#good-'+action[1]).text() > 1)){
                  $('#good-'+value).text(Number($('#good-'+value).text())-1);
                }else{
                  $('#good-'+value).text('');
                }
                $('#good-'+value).css('color','gray');
                break;
            }
            break;
          case ('favorite'):
            switch (data['result']){
              case 1:
                $('#favorite-'+value).text(Number($('#favorite-'+value).text())+1);
                $('#favorite-'+value).css('color','orange');
                break;
              case 2:
                if(Number($('#favorite-'+value).text() > 1)){
                  $('#favorite-'+value).text(Number($('#favorite-'+value).text())-1);
                }else{
                  $('#favorite-'+value).text('');
                }
                $('#favorite-'+value).css('color','gray');
                break;
            }
            break;
          case ('delete'):
            switch (data['result']){
              case true:
                $('.post-id-'+value).hide();
                break;
              case false:
                alert('投稿が削除出来ませんでした。');
                break;
            }
            break;
        }
      }
      return false;
    }).fail(function(){
      console.error('投稿アクション失敗');
    });
  });

  /*
   * トップページ / ログイン部
   */
  //ログイン・新規登録アニメーション
  $("input[name='TCLType']").val(["login"]);

  //ログイン・新規登録
  $('input[name="TCLType"]').click(function(){
    var TCLMode = $("input[name='TCLType']:checked").val();
    if(TCLMode == 'login'){
      $("#TCLMode").html("ログイン");
      $(".TCLRegistExt").hide();
    }else if(TCLMode == 'regist'){
      $("#TCLMode").html("新規登録！");
      $(".TCLRegistExt").show();
    }
  });

  /*
   * ルーム / submit
   */
  $('#form_room_create').submit(function(event){
    event.preventDefault();
    postRoomAction('create');
  });

  $('#form_room_edit').submit(function(event){
    event.preventDefault();
    postRoomAction('edit');
  });

  /*
   * ？
   */
  $('#fileUpload').on("change", function() {
    //var file = this.files[0];
    var name = $('#fileUpload')[0].files[0].name;
    var uploadFile = $('#fileUpload');
    console.log(uploadFile[0].files);
  });

  /*
   * 共通 / 投稿装飾
   */
  $("#form_message").select( function () {
    $('#form_message_decolation').show();
  });
  $('#decolation_bold').click(function(){decoration_set('b');});
  $('#decolation_italic').click(function(){decoration_set('i');});
  $('#decolation_underline').click(function(){decoration_set('u');});
  $('#decolation_strike').click(function(){decoration_set('s');});
  $('#decolation_size_big').click(function(){decoration_set('B');});
  $('#decolation_size_small').click(function(){decoration_set('S');});
});


/*
 *****************************
 ** 関数 * 150630 ************
 *****************************
 */

/*
 * 共通 / アラート部
 *  *doanimationが含まれる場合アラートを終了してから再開
*/
function twisterAlert(message, type, timeout){
  if($("#twisterAlert").hasClass('doanimation')){
    $("#twisterAlert").hide();
    $("#twisterAlert").removeClass("doanimation");
    $("#twisterAlert").removeClass("alert-"+alertType);
    $("#twisterAlert").html('');
  }
  if(message != null){
    (type == null) ? type='success' : type=type;
    (message == null) ? message='test message' : message=message;
    (timeout == null) ? timeout=1500 : timeout=timeout;
    alertType = type;

    $("#twisterAlert").show();
    $("#twisterAlert").html(message);
    $("#twisterAlert").addClass("doanimation");
    $("#twisterAlert").addClass("alert-"+type);
    setTimeout(function(){twisterAlert();},timeout);
  }
  return false;
}

/*
 * 共通 / ユーザーステータス取得
 */
function getUserStatus(get_id){
  var get_mode = (control == 'user') ? 'user_status' : 'profile_status';

  $.ajax({
    url: TwiURL + "api/user_status.json" + "?" + parseInt((new Date)/1000),
    datatype: 'json',
    type: "GET",
    data: {id:id, mode:get_mode}
  }).done(function(data){
    if(data['result'] == true){
      if(get_mode == 'profile_status'){
        $('.profile_box_user_post').text(data['return']['post']);
        $('.profile_box_user_level').text(data['return']['level']);
        $('.profile_box_user_exp').text(data['return']['exp']);
      }else if(get_mode == 'user_status'){
        //$('#profile_box_user_post').text(data['return']['post']);
        //$('#profile_box_user_level').text(data['return']['level']);
        //$('#profile_box_user_exp').text(data['return']['exp']);
      }
    }else{
      twisterAlert('ステータス取得失敗: ' +data['message'], 'danger', 5000);
    }
  }).fail(function(data){
    twisterAlert('ステータス取得失敗: 内部エラーが発生している可能性があります。', 'danger', 5000);
  });
}

/*
 * [要修正]
 * 共通 / 投稿読込
 *  getReadPost(); → 新着通知あり
 *  getReadPost('admin_fanclub'); → 新着通知なし
 */
function getReadPost(my){
  var shift;

  if(readSwitch == true){
    $.ajax({
      url: TwiURL + "api/Read_Post.json" + "?" + parseInt((new Date)/1000),
      datatype: 'json',
      type: "GET",
      data: {control:control, select:select, mode:mode, page:page}
    }).done(function(data){
      if(data['result'] == true){
        if(data['return'] != false){
          if(returnCount == 0){
            returnFirst = data['return'][0]['id'];
            returnArray = data['return'];
          }

          if(data['return'][0]['id'] != returnArray[0]['id'] || returnCount == 0 || page != 1){
            console.log('書換');
            $('#room_post_out').html(postMake(data['return']));
          }

          if(data['return'][0]['id'] != returnFirst && returnTitle == 0 && returnCount != 0 && document.hidden){
            returnTitle = 1;
            $("title").prepend('(新着あり) ');
          }

          postDateUpdate();
          returnArray = data['return'];
          returnFirst = data['return'][0]['id'];
          returnCount++;
        }else{
          $(".read_status .loading").hide();
          $(".read_status .no_result").show();
        }
      }else{
        twisterAlert('投稿取得失敗: ' +data['message'], 'danger', 5000);
      }
    }).fail(function(data){
      twisterAlert('投稿取得失敗: 内部エラーが発生している可能性があります。', 'danger', 5000);
    });
  }
}

/*
 * 共通 / ルーム読込
 *  getReadRoom(); → 新着通知あり
 */
function getReadRoom(){
  if(readSwitch == true){
    $.ajax({
      url: TwiURL + "api/Read_Room.json" + "?" + parseInt((new Date)/1000),
      datatype: 'json',
      type: "GET",
      data: {select:select, page:page}
    }).done(function(data){
      if(data['result'] == true){
        if(data['return'] != false){
          if(returnCount == 0){
            console.log(data['return']);
            returnFirst = data['return'][0]['post']['id'];
            returnArray = data['return'];
          }

          if(data['return'][0]['post']['id'] != returnArray[0]['post']['id'] || returnCount == 0 || page != 1){
            console.log('書換');
            $('#room_out').html(roomMake(data['return']));
            $('.post_area .title').html(data['rows'] + '個のルーム')
          }

          if(data['return'][0]['post']['id'] != returnFirst && returnTitle == 0 && returnCount != 0 && document.hidden){
            returnTitle = 1;
            $("title").prepend('(新着あり) ');
          }

          postDateUpdate();
          returnArray = data['return'];
          returnFirst = data['return'][0]['post']['id'];
          returnCount++;
        }else{
          $(".read_status .loading").hide();
          $(".read_status .no_result").show();
          $('.post_area .title').html('表示結果はありません。');
        }
      }else{
        twisterAlert('投稿取得失敗: ' +data['message'], 'danger', 5000);
      }
    }).fail(function(data){
      twisterAlert('投稿取得失敗: 内部エラーが発生している可能性があります。', 'danger', 5000);
    });
  }
}

/*
 * ルーム / ルーム作成･編集
 */
function postRoomAction(type){
  if(type == 'create'){
    var formObject = $('#form_room_create');
  }else if(type == 'edit'){
    var formObject = $('#form_room_edit');
  }
  $.ajax({
    url: TwiURL + "api/room_control.json",
    type: "POST",
    data: formObject.serializeArray()
  }).done(function(data){
    console.log(data);
    if(data['result'] == true){
      if(type == 'create'){
        location.href(TwiURL + 'room/' + data['return']);
      }else if(type == 'edit'){
        location.reload();
      }
    }
    return false;
  }).fail(function(data){
    alert(data['message']);
    return false;
  });
}

/*
 * 未完成
 */
function leftChange(){
  html = $('.left').html();
  $('.center').html(html);
}

/*
 * 共通 / 新着表示チェック
 */
function displayCheck(){
  document.addEventListener('webkitvisibilitychange', function(){
    if ( !document.webkitHidden ) {
      returnTitle = 0;
      //$("title").prepend('O');
      txt = $("title").text();
      $("title").text(txt.replace(/\(新着あり\)/g,""));
    }
  }, false);
  document.addEventListener('visibilitychange', function(){
    if ( !document.hidden ) {
      returnTitle = 0;
      txt = $("title").text();
      //$("title").prepend('O');
      $("title").text(txt.replace(/\(新着あり\)/g,""));
    }
  }, false);
}

/*
 * 共通 / 投稿時間更新
 */
function postDateUpdate(){
  $("span.postDate").each(function() {
    postDate(this);
  });
  $("span.postDateCourse").each(function() {
    postDateCourse(this);
  });
  return false;
}

/*
 * 共通 / 時間整形
 */
function postDate(element){
  var str = element.id;
  var objDate = new Date(str * 1000);
  var nowDate = new Date();
  var month = objDate.getMonth() + 1;
  var date = objDate.getDate();
  var hours = objDate.getHours();
  var minutes = objDate.getMinutes();
  var seconds = objDate.getSeconds();
  if ( hours < 10 ) { hours = "0" + hours; }
  if ( minutes < 10 ) { minutes = "0" + minutes; }
  if ( seconds < 10 ) { seconds = "0" + seconds; }
  if((nowDate - objDate) < 10800000){
    str = hours + ':' + minutes;
  }else{
    str = month + '/' + date + ' ' + hours + ':' + minutes;
  }
  $(element).html(str);
  return false;
}

/*
 * 共通 / 時間経過
 */
function postDateCourse(element){
  var str = '';
  var diff = Math.floor((new Date() / 1000)) - element.id;

  if(diff < 61){
    str = diff + '秒';
  }else if(diff < 3600){
    diff = Math.floor(diff / 60);
    str = diff + '分';
  }else if(diff < 86400){
    diff = Math.floor(diff / 3600);
    str = diff + '時間';
  }else if(diff > 86401){
    diff = Math.floor(diff / 86400);
    str = diff + '日';
  }
  $(element).html(str + '前');
  return false;
}

/*
 * 共通 / 投稿整形
 */
function postMake(post){
  var html = '';
  var addRoom = '';
  var goodHtml = '';
  var systemHtml = '';
  var systemClass = '';
  var good = [];
  var favorite = [];

  $.each(post, function(index, post){
    good = [];
    good['count'] = '';
    favorite = [];
    deleteHtml = '';

    // パブリック、ユーザー投稿の場合はルームのリンクを追加
    addRoom = ((control == 'public' || control == 'user') && post['room']['id'] != 1) ? '<a href="' + TwiURL + 'room/' + post['room']['name'] + '" target="_blank" class="public_display_room_name">★' + post['room']['name'] + '</a>' : '';

    // Good
    if(post['post']['good'] != null){
      good['count'] = 0;
      $.each(post['post']['good'], function(indep, posv){
        good['count']++;
        if(posv['user_id'] == id){ // 自分のID
          good['css'] = 'orange';
        }
      });
      good['count'] = (good['count'] == 0) ? '' : good['count'];
    }

    // ふぁぼ
    if(post['post']['favorite'] != null){
      favorite['count'] = 0;
      $.each(post['post']['favorite'], function(indep, posv){
        favorite['count']++;
        if(posv['user_id'] == id){
          favorite['css'] = 'orange';
        }
      });
      favorite['count'] = (favorite['count'] == 0)?'':favorite['count'];
    }

    // 自分の投稿
    deleteHtml = (post['user']['my'] == 1) ? '<span id="delete-' + post['id'] + '" class="js-post-action glyphicon glyphicon-remove action" style="font-size:14px;padding:2px 10px;"></span>' : '';

    // システム投稿
    systemClass = (post['post_system'] == 1) ? ' system' : '';
    systemHtml = (post['post_system'] == 1) ? ' / システム メッセージ' : '';

    // 生成
    html += '<div class="boxWrapper post user-id-' + post['user']['uid'] + ' post-id-' + post['id'] + '">';
      html += '<div class="postFace">';
        html += '<img src="http://data.kzho.net/icon/user/' + post['user']['uid'] + '/50x.png" class="postImg">';
      html += '</div>';
      html += '<div class="postBody">';
        html += '<div class="postContent">';
          html += '<div class="postInfo">';
            html += '<div class="postUser">';
              html += '<a href="' + TwiURL + 'user/' + post['user']['id'] + '"><span class="user_name">' + post['user']['name'] + '</span> <span class="user hidden-xs" style="color:#B3B3B3">@' + post['user']['id'] + '</span></a> <span class="level">Lv.' + post['user']['level'] + '<span class="exp">(Exp.' + post['user']['exp'] + ')</span></span>';
            html += '</div>';
            html += '<div class="postDate">';
              html += '<a href="' + TwiURL + 'post/' + post['id'] + '" target="_blank"><span class="postDateCourse" id="'+ post['date'] +'">' + '計算中…' + '</span></a>';
            html += '</div>';
          html += '<div class="clearBoth"></div>';
          html += '</div>';
          html += '<div class="postArticle' + systemClass + '">';
            html += post['post']['message'];
          html += goodHtml + '</div>';
          html += '<div class="postAction">';
            html += '<div class="pull-left">';
              html += '<span class="postDate" id="' + post['date'] + '" style="font-size:14px; color:gray;">';
                html += '計算中…';
              html += '</span>';
              html += ' ' + addRoom;
            html += '</div>';
            html += '<div class="pull-right actionButton" style="text-align:right;">';
              html += '<span id="mes-' + post['id'] + '" class="action_message"></span>';
              html += '<div class="post-action">';
                html += '<span id="good-' + post['id'] + '" class="js-post-action glyphicon glyphicon-thumbs-up action ' + good['css'] + '" style="font-size:14px;padding:2px 10px;"><b>' + good['count'] + '</b></span>';
                html += '<span id="favorite-' + post['id'] + '" class="js-post-action glyphicon glyphicon-star action ' + favorite['css'] + '" style="font-size:14px;padding:2px 10px;"><b>' + favorite['count'] + '</b></span>';
                //html += '<span id="reply-' + post['id'] + '" class="js-post-action glyphicon glyphicon-share-alt action" style="font-size:14px;padding:2px 10px;"><b></b></span>';
                html += deleteHtml;
              html += '</div>';
            html += '</div>';
            html += '<div class="clearBoth"></div>'
          html += '</div>';
        html += '</div>';
      html += '</div>';
    html += '</div>';
  });
  if(post.length > 1) html += '<div class="nextpage"></div>';
  return html;
}

/*
 * 共通 / ルーム一覧整形
 */
function roomMake(data){
  var html = '';
  var addRoom = '';
  var goodHtml = '';
  var systemHtml = '';
  var systemClass = '';

  $.each(data, function(index, data){
    html += '<div class="boxWrapper post">';
      html += '<div class="postFace">';
        html += '<img src="http://data.kzho.net/icon/user/' + data['user']['uid'] + '/50x.png" class="postImg">';
      html += '</div>';
      html += '<div class="postBody">';
        html += '<div class="postContent">';
          html += '<div class="postInfo">';
            html += '<div class="postUser">';
              html += '<a href="' + TwiURL + 'room/' + data['room']['name'] + '"><span class="room_name">' + data['room']['name'] + '</span></a>';
            html += '</div>';
            html += '<div class="postDate">';
              html += '<a href="http://twit.beluga.fm/home"><span class="postDateCourse" id="'+ data['post']['created_at'] +'">' + '計算中…' + '</span></a>';
            html += '</div>';
          html += '<div class="clearBoth"></div>';
          html += '</div>';
          html += '<div class="postArticle' + systemClass + '">';
            html += data['room']['desc'];
          html += goodHtml + '</div>';
          html += '<div class="postAction">';
            html += '<div class="pull-left">';
              html += '<span class="postDate room" id="' + data['post']['created_at'] + '" >';
                html += '計算中…';
              html += '</span>';
              html += '<span class="room_info"> / ' + data['room']['category'] + ' / ' + data['user']['name'] + '</span>';
            html += ' ' + addRoom;
            html += '</div>';
            /*
            html += '<div class="pull-right actionButton" style="text-align:right;">';
              html += '<span id="mes-' + post['id'] + '" class="action_message"></span>';
              html += '<div class="post-action">';
                html += '<span id="good-' + post['id'] + '" class="js-post-action glyphicon glyphicon-thumbs-up action" style="font-size:14px;padding:2px 10px;"></span>';
                html += '<span id="favorite-' + post['id'] + '" class="js-post-action glyphicon glyphicon-star action" style="font-size:14px;padding:2px 10px;"></span>';
                html += '<span id="reply-' + post['id'] + '" class="js-post-action glyphicon glyphicon-share-alt action" style="font-size:14px;padding:2px 10px;"></span>';
                html += '<span id="delete-' + post['id'] + '" class="js-post-action glyphicon glyphicon-remove action" style="font-size:14px;padding:2px 10px;"></span>';
              html += '</div>';
            html += '</div>';
            */
            html += '<div class="clearBoth"></div>'
          html += '</div>';
        html += '</div>';
      html += '</div>';
    html += '</div>';
  });
  if(data.length > 1) html += '<div class="nextpage"></div>';
  return html;
}

/*
 * 投稿 / デコレーション
 */
function decoration_set(tag){
  $('#form_message')
    .selection('insert', {text: "[" + tag + "]", mode: 'before'})
    .selection('insert', {text: "[/" + tag + "]", mode: 'after'});
  $('#form_message_decolation').hide();
}